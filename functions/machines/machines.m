%% machines
% This function provides the information about the treatment machine. 
%%

%% Syntax
% |Plan = machines(Plan)|

%% Description
% |Plan = machines(Plan)| fills in the |Plan| structure with the machine
% information (VDSA and SnoutPosition) for the machine with name
% |Plan.Machine.name|. 

%% Input arguments
% |Plan| - _struct_ - MIROpt structure where all the plan parameters are stored, without machine information.

%% Output arguments
% |Plan| - _struct_ - MIROpt structure where all the plan parameters are stored, including the machine information.

%% NOTES
% Only one machine is supported so far: IBA P2 machine from UPenn center.

%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu


function Plan = machines(Plan)

    if strcmp(Plan.Machine.name, 'P2')
        Plan.Machine.VDSA = [2234.8;1859.1];  %mm
        Plan.Machine.SnoutPosition = 430; %mm
    end
end