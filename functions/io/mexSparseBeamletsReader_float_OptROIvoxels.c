/*
compile with matlab: mex -largeArrayDims mexSparseBeamletsReader_float_OptROIvoxels.c
run with matlab: Beamlets = mexSparseBeamletsReader('Sparse_Dose.bin', [256 256 71], 8372,x);
x is a vector with the voxel indices
*/

#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <string.h>    /* memcpy */

/* undocumented function prototype */
EXTERN_C mxArray *mxCreateSparseNumericMatrix(mwSize m, mwSize n, 
    mwSize nzmax, mxClassID classid, mxComplexity ComplexFlag);

/*                  NbrOutputs  Outputs             NbrInputs   Inputs  */
void mexFunction(   int nlhs,   mxArray *plhs[],    int nrhs,   const mxArray *prhs[]){
    
    if(nrhs < 4) mexErrMsgIdAndTxt("mexSparseReader:nrhs", "This function takes 4 input arguments: file name, Dose grid size, Number of spots and voxel indices");
    else if(nlhs > 1) mexErrMsgIdAndTxt("mexSparseReader:nlhs", "Too many output arguments.");
    
    /* first input must be a string */
    if(mxIsChar(prhs[0]) != 1) mexErrMsgIdAndTxt( "mexSparseReader:inputNotString", "First input must be a string.");
    if(mxGetM(prhs[0])!=1) mexErrMsgIdAndTxt( "mexSparseReader:inputNotVector", "First input must be a row vector.");
    char *FileName;
    FileName = mxArrayToString(prhs[0]);
    if(FileName == NULL) mexErrMsgIdAndTxt( "mexSparseReader:InputConversionFailed", "Could not convert input to string.");
  
    /* second input must be a numeric array */
    if(mxGetNumberOfElements(prhs[1]) != 3) mexErrMsgIdAndTxt( "mexSparseReader:InvalidInputArray", "Dose grid sizes must contain 3 elements.");
    double DoseGridSize[3];
    DoseGridSize[0] = *(mxGetPr(prhs[1])+0);
    DoseGridSize[1] = *(mxGetPr(prhs[1])+1);
    DoseGridSize[2] = *(mxGetPr(prhs[1])+2);
    int NbrVoxels = (int)DoseGridSize[0]*DoseGridSize[1]*DoseGridSize[2];

    /* third input must be numeric */
    if(mxIsNumeric(prhs[2]) != 1) mexErrMsgIdAndTxt( "mexSparseReader:InvalidNbrSpots", "Third argument (number of spots) must be numeric.");
    int NbrSpots = (int)mxGetScalar(prhs[2]);
    
    /* fourth input must be a logical vector*/
    if (! mxIsLogical(prhs[3]) || mxGetN(prhs[3]) != 1 ) {
        mexErrMsgTxt("Fourth argument should be an logical vector (double precision).");
    } 
    mxLogical *x;
    x = mxGetLogicals(prhs[3]); /*input vector*/
    
    if(mxGetM(prhs[3]) != NbrVoxels) {
        mexErrMsgTxt("Dimensions of matrix and vector do not match."); /* check that the vector has the size equal to the number of voxels*/
    }
    
    /* Create the sparse data */
    double percent_sparse = 0.001; /* initial for mexSparseBeamletsReader was 0.009*/
    mwSize NonZeroMax = (mwSize)ceil((double)NbrVoxels * (double)NbrSpots * percent_sparse);
    mwSize oldNonZeroMax;
    
    
    float *Sparse_data = mxCalloc(NonZeroMax, sizeof(float));
    mwIndex *Sparse_index = mxCalloc(NonZeroMax, sizeof(mwIndex));
    mwIndex *Sparse_JC = mxCalloc(NbrSpots, sizeof(mwIndex)); /* Sparse_index of the first Non Zero value in each column */
    mwIndex NbrElements = 0;
        
    FILE *fid = NULL;
    fid = fopen(FileName, "rb");
    
    if(fid == NULL) mexErrMsgIdAndTxt( "mexSparseReader:UnableToOpenFile", "Unable to open the binary file.");
    
    uint32_t NonZeroVoxels, BeamID, LayerID, NbrContinuousValues, ReadVoxels, FirstIndex;
    float xcoord, ycoord;
    size_t NbrBytesRead;
    int i, j;
    float *data = mxCalloc(NbrVoxels, sizeof(float));
    mwSize NonZeroElements;
    

    
    for(i=0; i<NbrSpots; i++){
        
        /* mexPrintf("Spot %d / %d \n", i+1, NbrSpots); */
        
        NbrBytesRead = fread(&NonZeroVoxels, sizeof(uint32_t), 1, fid);
        /*NonZeroElements = NonZeroElements + NonZeroVoxels; */
        NbrBytesRead = fread(&BeamID, sizeof(uint32_t), 1, fid);
        NbrBytesRead = fread(&LayerID, sizeof(uint32_t), 1, fid);
        NbrBytesRead = fread(&xcoord, sizeof(float), 1, fid);
        NbrBytesRead = fread(&ycoord, sizeof(float), 1, fid);
    
        ReadVoxels = 0;
        Sparse_JC[i] = NbrElements;
        
  
        while(1){
            NbrBytesRead = fread(&NbrContinuousValues, sizeof(uint32_t), 1, fid);
            ReadVoxels += NbrContinuousValues;
            
            NbrBytesRead = fread(&FirstIndex, sizeof(uint32_t), 1, fid);
            FirstIndex = NbrVoxels - FirstIndex;
            
            NbrBytesRead = fread(data, sizeof(float), NbrContinuousValues, fid);
            
            for(j=0; j < NbrContinuousValues; j++){
                
                if(NbrElements >= NonZeroMax){
                    
                    
                    oldNonZeroMax = NonZeroMax;
                    percent_sparse += 0.001;
                    NonZeroMax = (mwSize)ceil((double)NbrVoxels * (double)NbrSpots * percent_sparse);
                                        
                    mexPrintf("Realloc %d -> %d \n", oldNonZeroMax, NonZeroMax); 
                    
                    /* make sure nzmax increases atleast by 1 */
                    if (NonZeroMax <= oldNonZeroMax) NonZeroMax = oldNonZeroMax + 1;
                    mexPrintf("NonZeroMax %d \n", NonZeroMax); 
                                      
                    Sparse_data = mxRealloc(Sparse_data,NonZeroMax*sizeof(float));
                    if(Sparse_data == NULL) mexPrintf( "null pointer, not enough heap memory");
                    Sparse_index = mxRealloc(Sparse_index, NonZeroMax*sizeof(mwIndex));
                }
                
                if (x[FirstIndex-j-1]){
                    Sparse_data[NbrElements] = data[j];
                    Sparse_index[NbrElements] = FirstIndex-j-1;
                    NbrElements ++;
                }
                
            }
            
            if(ReadVoxels >= NonZeroVoxels) break;
            
            
        }
        
        
        
  
    } /*end loop over spots*/
    /*mexPrintf("NonZeroElements %d \n", NonZeroElements); */
    Sparse_JC[NbrSpots] = NbrElements;
    
   
    
    /* create sparse*/
    mexPrintf("NbrElements %d \n", NbrElements);
    /*mexPrintf("sizeof(mwIndex) %d \n", sizeof(mwIndex));*/
    
    plhs[0] = mxCreateSparseNumericMatrix(NbrVoxels,NbrSpots,NbrElements,mxSINGLE_CLASS,mxREAL);
    
    memcpy((void*)mxGetPr(plhs[0]), (const void*)Sparse_data, NbrElements*sizeof(float));
    memcpy((void*)mxGetIr(plhs[0]), (const void*)Sparse_index, NbrElements*sizeof(mwIndex));
    memcpy((void*)mxGetJc(plhs[0]), (const void*)Sparse_JC, (NbrSpots+1)*sizeof(mwIndex));
    
    mxFree(data);
    fclose(fid);
}
