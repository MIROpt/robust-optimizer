function [ Sparse_info, Sparse_beamlets ] = Import_Sparse_Beamlets_double( WorkDir, Plan_info, DoseScaling, FileName,OptROIVoxels )
% last modification 27th January 2016

%   [CurrentPath,~,~] = fileparts(mfilename('fullpath'));
%   addpath([CurrentPath '/lib']);
  
  if(nargin<4)
      FileName = '/Outputs/Sparse_Dose.txt';
  else
      FileName = fullfile('Outputs', FileName);
  end
  
  if(strcmpi(FileName((end-3):end), '.txt') ~= 1)
      error(['File ' fullfile(WorkDir, FileName) ' is not a valid sparse matrix header!'])
  end
  
  if(exist(fullfile(WorkDir, FileName)) == 0)
      error(['File ' fullfile(WorkDir, FileName) ' not found!'])
  end
  
  % Read sparse beamlets header file
  disp(['Read sparse beamlets: ' fullfile(WorkDir, FileName)]);
  Sparse_info = Sparse_read_header(fullfile(WorkDir, FileName));
  
  if(sum(strcmp(Sparse_info.SimulationMode, 'Beamlet')) < 1)
      error(['Not a beamlet file'])
  end
  
  % Read sparse beamlets binary file
  BinFile = fullfile('Outputs', Sparse_info.BinaryFile);
  disp(['Read binary file: ' fullfile(WorkDir, BinFile)]);
  Sparse_beamlets = mexSparseBeamletsReader_double_OptROIvoxels(fullfile(WorkDir, BinFile), Sparse_info.ImageSize, Sparse_info.NbrSpots,OptROIVoxels);
  
  
  % Calculate delivered protons for each beamlet and convert dose values in Gray units
  % spot weight is always = 1 in beamlet mode
  Spot = 0;
  for f=1:length(Plan_info.Beam_data);
    for j=1:length(Plan_info.Beam_data{f}.spots)
      for k=1:length(Plan_info.Beam_data{f}.spots(j).weight)
        Spot = Spot + 1;
        Sparse_beamlets(:,Spot) = Sparse_beamlets(:,Spot) * 1.602176e-19 * 1000 * MU_to_NumProtons(1, Plan_info.Beam_data{f}.spots(j).energy) .* Plan_info.OriginalHeader.FractionGroupSequence.Item_1.NumberOfFractionsPlanned .* DoseScaling;
      end
    end
  end
  
end