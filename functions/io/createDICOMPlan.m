%% createDICOMPlan
% This function writes an RT-DICOM plan (.dcm), containing all the beam 
% parameters and machine configuration required for a given treatment.
% So far, only Pencil Beam Scanning (PBS) modality is supported, which
% comprises information about the energy layers, spot positions, spot 
% weights, beam angles, etc.
%%

%% Syntax
% |createDICOMPlan(Plan, CTinfo, dataPath)|

%% Description
% |createDICOMPlan(Plan, CTinfo, dataPath)| returns a RT-DICOM plan 
% containing the beam parameters in |Plan|, associated to the patient info
% in |CTinfo|. The .dcm file is writen in the path |dataPath|.

%% Input arguments
% |Plan| - _struct_ - MIROpt data structure containing the plan parameters.
% The following data must be present in the structure:
%
% * |Plan.name| - _string_ - Name of the RT plan.
% * |Plan.TargetROI_ID| - _scalar_ - Index of the target volume, as it
%   appears in the RTSTRUCT list.
% * |Plan.fractions| - _scalar_ - Number of treatment fractions.
% * |Plan.Beams| - _struct_ - Structure containing the information about the proton beam with index i. The following data must be present:
% * |Plan.Machine| - _struct_ - Information about the treatment machine, including the name (|name|), the virtual distance from source to axis (|VDSA|) and the snout position (|SnoutPosition|).
% * |Plan.FileName| - _string_ - Name of the RT plan file.
%
% |CTinfo| - _struct_ - Structure containing all the information stored in
% the CT DICOM header. The following data must be present:
% * |CTinfo.PatientID|- _string_ - Patient ID.
% * |CTinfo.PatientBirthDate| - _string_ - Patient birth day date.
% * |CTinfo.PatientSex| - _string_ - Patient sex.
%
% |dataPath| - _string_ - path where you want the RT-DICOM plan to be 
% located.

%% Output arguments
% none

%% Contributors
% Author(s): Ana Barragan, Lucian Hotoiu


function  createDICOMPlan( Plan, CTinfo, dataPath )

% Use MATLAB dicom dictionary 
dicomdict('set',[matlabroot '/toolbox/images/iptformats/dicom-dict.txt'])

% Patient and RTplan file info

                          info.Filename= Plan.name;
                       info.FileModDate= date;
                          info.FileSize= [];
                            info.Format= 'DICOM';
                     info.FormatVersion= [];
                             info.Width= [];
                            info.Height= [];
                          info.BitDepth= [];
                         info.ColorType= 'grayscale';
    info.FileMetaInformationGroupLength= [];
        info.FileMetaInformationVersion= [];
           info.MediaStorageSOPClassUID= '1.2.840.10008.5.1.4.1.1.481.8';% Radiation Therapy Ion Plan Storage check this: http://www.dicomlibrary.com/dicom/sop/
        info.MediaStorageSOPInstanceUID= dicomuid;
                 info.TransferSyntaxUID= '1.2.840.10008.1.2'; % Implicit VR Endian: Default Transfer Syntax for DICOM, check this http://www.dicomlibrary.com/dicom/transfer-syntax/
            info.ImplementationClassUID= '1.2.246.352.70.2.1.7'; 
         info.ImplementationVersionName= ['MATLAB', version];
              info.SpecificCharacterSet= 'ISO_IR 100';
              info.InstanceCreationDate= datestr(now,'yyyymmdd');
              info.InstanceCreationTime= datestr(now,'HHMMSS');
                       info.SOPClassUID= '1.2.840.10008.5.1.4.1.1.481.8'; % Radiation Therapy Ion Plan Storage check this: http://www.dicomlibrary.com/dicom/sop/
                    info.SOPInstanceUID= dicomuid;
                         info.StudyDate= datestr(now,'yyyymmdd');
                         info.StudyTime= datestr(now,'HHMMSS');
                   info.AccessionNumber= '';
                          info.Modality= 'RTPLAN';
                      info.Manufacturer= 'MIROpt';
            info.ReferringPhysicianName= struct('FamilyName','','GivenName','','MiddleName','','NamePrefix','','NameSuffix','');
             info.ManufacturerModelName= 'MIROpt';
                       info.PatientName= struct('FamilyName','','GivenName','','MiddleName','','NamePrefix','','NameSuffix','');
                         info.PatientID= CTinfo.PatientID;
                  info.PatientBirthDate= CTinfo.PatientBirthDate;
                        info.PatientSex= CTinfo.PatientSex;
            info.PatientIdentityRemoved= '';
            info.DeidentificationMethod= '';
                   info.SoftwareVersion= '';
                  info.StudyInstanceUID= dicomuid;
                 info.SeriesInstanceUID= dicomuid;
                           info.StudyID= '';
                      info.SeriesNumber= [];
               info.FrameOfReferenceUID= dicomuid;
        info.PositionReferenceIndicator= '';
                   info.SamplesPerPixel= 1;
         info.PhotometricInterpretation= 'MONOCHROME2';
                              info.Rows= 0;
                           info.Columns= 0;
                     info.BitsAllocated= 16;
                        info.BitsStored= 16;
                           info.HighBit= 15;
               info.PixelRepresentation= 0;
           info.SmallestImagePixelValue= [];
            info.LargestImagePixelValue= [];
                       info.RTPlanLabel= 'PBS';
                        info.RTPlanDate= '';
                        info.RTPlanTime= '';
                        info.PlanIntent= 'RESEARCH';
                    info.RTPlanGeometry= 'PATIENT';
                    
% Plan delivery parameters

             %info.DoseReferenceSequence = struct('Item_1',{});
             
% CHECK this: http://dicom.nema.org/medical/dicom/current/output/chtml/part03/sect_C.8.8.10.html
             
info.DoseReferenceSequence.Item_1.ReferencedROINumber = Plan.TargetROI_ID - 1; % because in ROI the first structure is empty ''
info.DoseReferenceSequence.Item_1.DoseReferenceNumber = 1; % I don't know exactly what does this means but in the RN_PBS.dcm was 1 (in RayStation plan was 0)
info.DoseReferenceSequence.Item_1.DoseReferenceUID = dicomuid;
info.DoseReferenceSequence.Item_1.DoseReferenceStructureType = 'SITE'; % types: POINT (dose reference point specified as ROI), VOLUME (dose reference volume specified as ROI), SITE (dose reference clinical site), COORDINATES (point specified by Dose Reference Point Coordinates (300A,0018))
info.DoseReferenceSequence.Item_1.DoseReferenceDescription = 'Primary prescription'; % user-defined
info.DoseReferenceSequence.Item_1.DoseReferenceType = 'TARGET'; % types: 'TARGET' and 'ORGAN_AT_RISK'
% info.DoseReferenceSequence.Item_1.TargetPrescriptionDose
% info.DoseReferenceSequence.Item_1.TargetUnderdoseVolumeFraction 

             %info.FractionGroupSequence= struct('Item_1',{});

info.FractionGroupSequence.Item_1.FractionGroupNumber = 1;
info.FractionGroupSequence.Item_1.NumberOfFractionsPlanned = Plan.fractions;
info.FractionGroupSequence.Item_1.NumberOfBeams = length(Plan.Beams);
info.FractionGroupSequence.Item_1.NumberOfBrachyApplicationSetups = 0;

meterset = cell(length(Plan.Beams),1);
cumulative_meterset = cell(length(Plan.Beams),1);
total_meterset = cell(length(Plan.Beams),1);
    
for i = 1:length(Plan.Beams)
    itemBeam = sprintf('Item_%i',i);
    info.FractionGroupSequence.Item_1.ReferencedBeamSequence.(itemBeam).BeamDose = 1; % It is used by FoCa in PBSPlan line 116 as Field weight, I set to 1 so that all the beams have the same weigth
    
    % Compute cumulative metersets (Monitor Units)
    for j=1:length(Plan.Beams(i).Layers)
        meterset{i}(j) = sum(Plan.Beams(i).Layers(j).SpotWeights);
    end
    cumulative_meterset{i} = cumsum(meterset{i});
    total_meterset{i} = sum(meterset{i});
          
    info.FractionGroupSequence.Item_1.ReferencedBeamSequence.(itemBeam).BeamMeterset = total_meterset{i}; % number of MU's for the beam , for the moment I assume that a 1 weight = 1 MU, so that BeamMeterset = FinalCumulativeMetersetWeight
    info.FractionGroupSequence.Item_1.ReferencedBeamSequence.(itemBeam).ReferencedBeamNumber = i;
end

             
              %info.PatientSetupSequence = {};
              
for i = 1:length(Plan.Beams)
  itemBeam = sprintf('Item_%i',i);
  info.PatientSetupSequence.(itemBeam).PatientPosition = 'HFS'; % most of the cases are HFS but ATTENTION can be also in the other way around
  info.PatientSetupSequence.(itemBeam).PatientSetupNumber = i;
  info.PatientSetupSequence.(itemBeam).SetupTechnique = ''; % 'ISOCENTRIC'
end


         %info.IonToleranceTableSequence = struct('Item_1',{});
            
info.IonToleranceTableSequence.Item_1.ToleranceTableNumber = []; %10049
info.IonToleranceTableSequence.Item_1.ToleranceTableLabel =''; %'CAM Head'
info.IonToleranceTableSequence.Item_1.GantryAngleTolerance = []; %0.2000
info.IonToleranceTableSequence.Item_1.BeamLimitingDeviceAngleTolerance = [];%0.2000
info.IonToleranceTableSequence.Item_1.BeamLimitingDeviceToleranceSequence = {}; % check in plan: BeamLimitingDevicePositionTolerance: 2, RTBeamLimitingDeviceType: 'X'
info.IonToleranceTableSequence.Item_1.SnoutPositionTolerance = []; %10
info.IonToleranceTableSequence.Item_1.PatientSupportAngleTolerance = []; %1
info.IonToleranceTableSequence.Item_1.TableTopPitchAngleTolerance = []; %3
info.IonToleranceTableSequence.Item_1.TableTopRollAngleTolerance = []; %3
info.IonToleranceTableSequence.Item_1.TableTopVerticalPositionTolerance = []; %10
info.IonToleranceTableSequence.Item_1.TableTopLongitudinalPositionTolerance = []; %10
info.IonToleranceTableSequence.Item_1.TableTopLateralPositionTolerance = []; %10      
         
                   info.IonBeamSequence = {};
                   
 for i = 1:length(Plan.Beams)
  itemBeam = sprintf('Item_%i',i);
  
  info.IonBeamSequence.(itemBeam).Manufacturer = 'IBA';
  info.IonBeamSequence.(itemBeam).InstitutionName = 'MIRO - ImagX - UCL';
  info.IonBeamSequence.(itemBeam).InstitutionalDepartmentName = 'MIRO';
  info.IonBeamSequence.(itemBeam).ManufacturerModelName = 'IBA';
  info.IonBeamSequence.(itemBeam).TreatmentMachineName = Plan.Machine.name; % room 2 for UPenn...
  info.IonBeamSequence.(itemBeam).PrimaryDosimeterUnit = 'MU';
  info.IonBeamSequence.(itemBeam).BeamNumber= i;
  info.IonBeamSequence.(itemBeam).BeamName=  Plan.Beams(i).name;
  info.IonBeamSequence.(itemBeam).BeamType = 'STATIC';
  info.IonBeamSequence.(itemBeam).RadiationType = 'PROTON';
  info.IonBeamSequence.(itemBeam).TreatmentDeliveryType = 'TREATMENT';
  info.IonBeamSequence.(itemBeam).NumberOfWedges = 0;
  info.IonBeamSequence.(itemBeam).NumberOfCompensators = 0;
  info.IonBeamSequence.(itemBeam).NumberOfBoli = 0;
  info.IonBeamSequence.(itemBeam).NumberOfBlocks = 0;
  info.IonBeamSequence.(itemBeam).FinalCumulativeMetersetWeight =  total_meterset{i};
  info.IonBeamSequence.(itemBeam).NumberOfControlPoints = length(Plan.Beams(i).Layers)*2; % Number of layers (*2 because they appear twice)
  info.IonBeamSequence.(itemBeam).ScanMode = 'MODULATED';
  info.IonBeamSequence.(itemBeam).VirtualSourceAxisDistances = Plan.Machine.VDSA; 
  info.IonBeamSequence.(itemBeam).SnoutSequence = struct('Item_1',struct('SnoutID','PBS Snout'));
  info.IonBeamSequence.(itemBeam).NumberOfRangeShifters = Plan.Beams(i).NumberOfRangeShifters; % check http://dicom.nema.org/medical/dicom/current/output/chtml/part03/sect_C.8.8.26.html
  if (Plan.Beams(i).NumberOfRangeShifters ~= 0)
      for rs = 1:Plan.Beams(i).NumberOfRangeShifters
          itemRS = sprintf('Item_%i',rs);
          info.IonBeamSequence.(itemBeam).RangeShifterSequence.(itemRS).RangeShifterNumber = rs;
          info.IonBeamSequence.(itemBeam).RangeShifterSequence.(itemRS).RangeShifterID = Plan.Beams(i).RSinfo(rs).RangeShifterID;
          info.IonBeamSequence.(itemBeam).RangeShifterSequence.(itemRS).RangeShifterType = Plan.Beams(i).RSinfo(rs).RangeShifterType;
      end
  end
  info.IonBeamSequence.(itemBeam).NumberOfLateralSpreadingDevices = 0;
  info.IonBeamSequence.(itemBeam).LateralSpreadingDeviceSequence = {}; % Check in plan:  Item_i.LateralSpreadingDeviceNumber: 1
                                                                       %                        LateralSpreadingDeviceID: 'MagnetX'
                                                                       %                        LateralSpreadingDeviceType: 'MAGNET'
  info.IonBeamSequence.(itemBeam).NumberOfRangeModulators = 0;
  info.IonBeamSequence.(itemBeam).PatientSupportType = 'TABLE';
  info.IonBeamSequence.(itemBeam).PatientSupportID = '';
  info.IonBeamSequence.(itemBeam).PatientSupportAccessoryCode = '';
  
  info.IonBeamSequence.(itemBeam).IonControlPointSequence = {};
  
  CMW = 0;
  counter = 1;
  
  for l = 1:length(Plan.Beams(i).Layers)
      
      % OJO layers appear twice in the dicom RTplan. The only difference is
      % the CumulativeMetersetWeight which is updated only in the second
      % version.
      
      % First layer occurrence
      
      
      itemLayer = sprintf('Item_%i',counter);
      
      
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).NominalBeamEnergyUnit = 'MEV';
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).ControlPointIndex = counter-1;
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).NominalBeamEnergy=  Plan.Beams(i).Layers(l).Energy;
      
      if l == 1
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).GantryAngle = Plan.Beams(i).GantryAngle;
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).GantryRotationDirection = 'NONE';
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).BeamLimitingDeviceAngle = 0;
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).BeamLimitingDeviceRotationDirection = 'NONE';
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).PatientSupportAngle = 0; % ???
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).PatientSupportRotationDirection = 'NONE';
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).TableTopVerticalPosition = [];
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).TableTopLongitudinalPosition = [];
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).TableTopLateralPosition = [];
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).IsocenterPosition = Plan.Beams(i).Isocenter; %%%%%%%%%%%%%%%% OJO check coords!
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).TableTopPitchAngle = 0;
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).TableTopPitchRotationDirection = 'NONE';
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).TableTopRollAngle = 0;
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).TableTopRollRotationDirection = 'NONE';
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).GantryPitchAngle = [];
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).GantryPitchRotationDirection = '';
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).SnoutPosition = Plan.Machine.SnoutPosition;
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).MetersetRate = []; % 200 in the PATIENT FoCa example
          
          if (Plan.Beams(i).NumberOfRangeShifters ~= 0)
              for rs = 1:Plan.Beams(i).NumberOfRangeShifters
                  itemRS = sprintf('Item_%i',rs);
                  info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).RangeShifterSettingsSequence.(itemRS).RangeShifterSetting = Plan.Beams(i).RSinfo(rs).RangeShifterSetting;
                  info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).RangeShifterSettingsSequence.(itemRS).IsocenterToRangeShifterDistance = Plan.Beams(i).RSinfo(rs).IsocenterToRangeShifterDistance;
                  info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).RangeShifterSettingsSequence.(itemRS).RangeShifterWaterEquivalentThickness = Plan.Beams(i).RSinfo(rs).RangeShifterWET;
                  info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).RangeShifterSettingsSequence.(itemRS).ReferencedRangeShifterNumber=rs;
              end
          end
          
          info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).LateralSpreadingDeviceSettingsSequence = {}; % check this in plan
          
                                                                              %                        for Item_i (for each beam).
                                                                              %     LateralSpreadingDeviceWaterEquivalentThickness: 0
                                                                              %                      LateralSpreadingDeviceSetting: '9.7711684342'
                                                                              %          IsocenterToLateralSpreadingDeviceDistance: 2.3385e+03
                                                                              %             ReferencedLateralSpreadingDeviceNumber: 1
          
      
      end
      
      
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).CumulativeMetersetWeight = CMW; 
      CMW = CMW + sum(Plan.Beams(i).Layers(l).SpotWeights);
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).ScanSpotTuneID = 'Spot';
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).NumberOfScanSpotPositions = numel(Plan.Beams(i).Layers(l).SpotWeights);
      
      pos = zeros(numel(Plan.Beams(i).Layers(l).SpotWeights)*2,1);
      pos(1:2:end-1) = Plan.Beams(i).Layers(l).SpotPositions(:,1); % X_BEV
      pos(2:2:end) = Plan.Beams(i).Layers(l).SpotPositions(:,2); % Y_BEV
      
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).ScanSpotPositionMap =  pos;
      
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).ScanSpotMetersetWeights = Plan.Beams(i).Layers(l).SpotWeights';
      % Check this with Daniel....
      % I assume a sigma of 4mm so that FWMH = 4*2.355= 9.4200 but this
      % should be properly calculated with the beam model at least for the
      % first layer which is the value used by FoCa in PBSPlan line 135.
      % Actually, for the FAST mode, only the value of the first layer is
      % used, while for the ROBUST mode, the spot sizes for each layer are
      % recalculated using the BeamData in flillFlu of DG
      FWMHx = 9.4200;
      FWMHy = 9.4200;
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).ScanningSpotSize = [FWMHx;FWMHy]; % [2x1 single]
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).NumberOfPaintings = 1;
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).Private_300b_10xx_Creator= 'IMPAC';
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).Private_300b_1017= [];
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer).ReferencedDoseReferenceSequence = struct('Item_1',{}); % check on plan
                                                                            %       CumulativeDoseReferenceCoefficient: 0.3756
                                                                            %          ReferencedDoseReferenceNumber: 1
                                                                            
      % Second layer occurrence
      
      counter = counter + 1;
      
      itemLayer2 = sprintf('Item_%i',counter);
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer2) = info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer);
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer2).ScanSpotMetersetWeights(:) = 0;
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer2).ControlPointIndex = counter-1;
      info.IonBeamSequence.(itemBeam).IonControlPointSequence.(itemLayer2).CumulativeMetersetWeight = CMW;
      
      counter = counter + 1;
      
  end
  
  
  info.IonBeamSequence.(itemBeam).Private_300b_10xx_Creator = 'IMPAC';
  info.IonBeamSequence.(itemBeam).Private_300b_1002 = []; % ????
  info.IonBeamSequence.(itemBeam).Private_300b_1004 = []; % ????
  info.IonBeamSequence.(itemBeam).Private_300b_100e = []; % ????
  info.IonBeamSequence.(itemBeam).ReferencedPatientSetupNumber = 1;
  info.IonBeamSequence.(itemBeam).ReferencedToleranceTableNumber = [];
  %info.IonBeamSequence.(itemBeam).Private_SpotSpacing = Plan.Beams(i).SpotSpacing;
end                  
                   
                   
    %info.ReferencedStructureSetSequence = struct('Item_1',{});
    
info.ReferencedStructureSetSequence.Item_1.ReferencedSOPClassUID = '1.2.840.10008.5.1.4.1.1.481.3'; %Radiation Therapy Structure Set Storage check this http://www.dicomlibrary.com/dicom/sop/
info.ReferencedStructureSetSequence.Item_1.ReferencedSOPInstanceUID = dicomuid;
    
    
                    info.ApprovalStatus= 'UNAPPROVED';
                   
data = [];

dicomwrite(data,fullfile(dataPath,[Plan.FileName,'.dcm']),info,'createmode','copy');

end

