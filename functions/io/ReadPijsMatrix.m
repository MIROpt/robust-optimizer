function [ Plan ] = ReadPijsMatrix( Plan, OptConfig, handles)

if isempty(Plan.PlanExistentFile)
    error('No path for PlanExistentFile has been introduced')
end

% load plan data in handles in case it is empty
% NOTE: there must be a Plan.dcm!
if(length(handles.plans.name)==1)
    % starts at 2 because the first one is always empty
    [handles.plans.data{2},handles.plans.info{2}] = load_DICOM_RT_Plan(fullfile(Plan.output_path,'Plan.dcm'));
end

% Copy into local variable format
Plan_info_w1 = handles.plans.info{2};
Plan_info_w1.Beam_data = handles.plans.data{2};

WorkDir = Plan.PlanExistentFile;

% Import beamlets for each scenario

for i_4D = 1:Plan.Nbr4DScenarios
    for rr = 1:Plan.NbrRandomScenarios
        folderName = 'Beamlets';
        if(Plan.Opt4Dmode == 1)
            folderName = [folderName,'_4D',num2str(i_4D)];
        end
        if (Plan.NbrRandomScenarios > 1)
            folderName = [folderName,'_random',num2str(rr)];
        end
        
        switch Plan.BeamletsBy
            
            case 'MCsquare'
                DoseList = dir(fullfile(WorkDir, folderName,'/Outputs/Sparse_Dose*.txt'));
                for rs = 1:Plan.NbrRangeScenarios
                    
                    % Read dose contribution matrix (given in Gy)
                    if (OptConfig.BeamletsMatrixPrecision == 'd')
                        % Read and Reduce matrix
                        if (rs == Plan.rs_nominal && rr == Plan.rr_nominal)
                            [~, Plan.Scenario4D(i_4D).RandomScenario(rr).RangeScenario(rs).P] = Import_Sparse_Beamlets_double(fullfile(WorkDir,folderName), Plan_info_w1, 1.0, DoseList(rs, 1).name,Plan.OptROIVoxels_nominal);
                        else
                            [~, Plan.Scenario4D(i_4D).RandomScenario(rr).RangeScenario(rs).P] = Import_Sparse_Beamlets_double(fullfile(WorkDir,folderName), Plan_info_w1, 1.0, DoseList(rs, 1).name,Plan.OptROIVoxels_robust);
                        end
                        
                    elseif (OptConfig.BeamletsMatrixPrecision == 'f')
                        % Read and Reduce matrix
                        if (rs == Plan.rs_nominal && rr == Plan.rr_nominal)
                            [~, Plan.Scenario4D(i_4D).RandomScenario(rr).RangeScenario(rs).P] = Import_Sparse_Beamlets_float(fullfile(WorkDir,folderName), Plan_info_w1, 1.0, DoseList(rs, 1).name,Plan.OptROIVoxels_nominal);
                        else
                            [~, Plan.Scenario4D(i_4D).RandomScenario(rr).RangeScenario(rs).P] = Import_Sparse_Beamlets_float(fullfile(WorkDir,folderName), Plan_info_w1, 1.0, DoseList(rs, 1).name,Plan.OptROIVoxels_robust);
                        end
                    end
                    
                end
                
            case '...'
               
        end
    end
end

end

