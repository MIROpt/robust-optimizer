%% MIROpt
% SET OPTIMIZATION PROBLEM - PBS proton treatment
%
% This script reads the CT image for a given patient and calculates the
% spot positions for the selected target volume according to the beam
% settings inserted by the user. Spot positions are sent to the dose engine (default MCsquare), where the dose for each beamlet is calculated.
%
% Plan information (including dose constraints) is sent to IPOPT solver
% (NLPsolver.m) which will find the spot weights that gives the optimal
% dose distribution.
%   
% A forward MC dose calculation is performed at the end by using MCsquare.
%%

%% Syntax
% |handles = MIROpt(handles)|
%
% |handles = MIROpt(handles, configFile, selected_ROIs, targetROI, use_gui)|

%% Description
% |handles = MIROpt(handles)| - This function is called with |one| argument when MIROpt is launched in GUI mode, via the REGGUI interface. The information for MIROpt to run is contained in the |handles| and the default |yaml| configuration file.
%
% |handles = MIROpt(handles, configFile, selected_ROIs, targetROI, use_gui)| - This function is called with |five| arguments in standalone (no GUI) mode, via the |start_MIROpt_workflow| function. All the configuration paramenters required by MIROpt are contained in the user |yaml| file.

%% Input arguments
% |handles| - _STRUCT_ - REGGUI data structure.
%
% |configFile| - _STRING_ - Path of the user specific yaml MIROpt configuration file.
%
% |selected_ROIs| - _CELL OF STRINGS_ - containes the contour names inside the dicom RT structure file.
%
% |targetROI| - _STRING_ - User defined target volume, among the contours in the RT structure file.
%
% |use_gui| - _STRING_ - boolean-behaved variable to decide whether MIROpt should launch in graphical mode or in one-shot batch run.

%% Output arguments
% |handles| - _STRUCT_ - MIROpt returns the REGGUI data structure that contains the computed DVHs and the optimized plan image containing the dose.

%% Contributors
% Authors : Ana Maria Barragan Montero, Lucian Hotoiu


% Main function
%--------------------------------------------------------------------------
function handles = MIROpt(handles, configFile, selected_ROIs, targetROI, use_gui)   
    
    if(nargin==1) % call from regguiC (only handles as input argument)
        % Select rtstruct
        if(length(handles.images.name)>2)
            [rtstruct_names,~,selectIdx,~] = Image_list(handles,'Select ROIs for optimization',1,1);
            selected_ROIs = rtstruct_names;            
            
            [rtstruct_name,~,selectIdx,~] = Image_list(handles,'Select target ROI',1);
            targetROI = rtstruct_name;
        else
            disp('No rtstruct available. Abort')
            return
        end
        
        % Select image
        [image_name,~] = Image_list(handles,'Select associated CT',1);   

        configFile_default = which('config_MIROpt_default.yaml');
        handles = {['handles = MIROpt(handles,''', configFile_default, ''',' cell2str(selected_ROIs), ',''' targetROI, ''',''gui'');']};
        return
    end
   
    % Get configuration data and convert certain cells2mat for MIROpt compatibility 
    config = ReadYaml(configFile);
    config = manageData(config, handles);
    
    % Define machine in the final plan
    config.Plan = machines(config.Plan);
    
    % Don't show computed DVHs by default
    show_dvh = 0;
    
    
    % Call from MIROpt interface
    if(nargin>4)
        if(strcmp(use_gui,'gui'))            
            % Set default gui values
            default_gui_input = {config.Plan, config.OptConfig, config.Outputs, config.Rtest};            
            
            % Add user ROIs to default input
            default_gui_input{end+1} = selected_ROIs;
            
            % Add user target ROI to default input
            default_gui_input{end+1} = targetROI;
            
            % Launch MIROpt GUI
            MIROpt_gui(handles, default_gui_input);
            return;            
        elseif(strcmp(use_gui,'show_dvh'))
            % show dvh at the end of computation
            show_dvh = 1;
        end
    end
    
    
    % Update user input
    config = manageOuput(config, handles);
    
    Plan = config.Plan;
    OptConfig = config.OptConfig;
    Outputs = config.Outputs;
    Rtest = config.Rtest;


    % Create ROI specific properties for all structures
    for i = 1:length(selected_ROIs)  
        % Initialize number of voxels
        ROI(i).nvoxels = [];
        % Initialize voxel ID
        ROI(i).voxelsID = [];
        % Set ROI name and create list of ROIs
        ROI(i).name = selected_ROIs{i};
        % Calculate mask for each ROI
        ROI(i).mask3D.value = Get_reggui_data(handles, selected_ROIs{i},'images');
        % Convert to 1D sparse mask with z inverse format (to be consistent 
        % with beamlets format)
        temp = flipdim(ROI(i).mask3D.value,3);
        ROI(i).mask1D = sparse(logical(double(temp(:))));
    end
    
    
    % Set target volume idx
    targetROI_idx = find(ismember(selected_ROIs, targetROI));
    Plan.TargetROI_ID = targetROI_idx;
 
    
    % Load CT DICOM info and DoseGrid
    Plan.DoseGrid.size = size(handles.images.data{2});
    Plan.DoseGrid.resolution = handles.images.info{2}.Spacing ; % mm
    Plan.DoseGrid.nvoxels = Plan.DoseGrid.size(1) * Plan.DoseGrid.size(2) * Plan.DoseGrid.size(3);
    
    
    % Set original Dicom header
    Plan.CTinfo = handles.images.info{2}.OriginalHeader;   
    % Set ImagePositionPatient as the image corner
    Plan.CTinfo.ImagePositionPatient = Plan.CTinfo.ImagePositionPatient;
    
    
    % SET BEAM and SCANNING GRID (spot positions and energy layers)
    TVonCT = ROI(targetROI_idx).mask3D.value; % 3D mask for selected target volume (TV)
    
    imgCT_RTSTRUCT.dval = handles.images.data{2};
    imgCT_RTSTRUCT.ddim = Plan.DoseGrid.size;
    imgCT_RTSTRUCT.dspa = Plan.DoseGrid.resolution;
    

    imgCT_RTSTRUCT.dpcx = Plan.CTinfo.ImagePositionPatient(1) + [0:Plan.DoseGrid.size(1)-1]' * Plan.DoseGrid.resolution(1);
    imgCT_RTSTRUCT.dpcy = Plan.CTinfo.ImagePositionPatient(2) + [0:Plan.DoseGrid.size(2)-1]' * Plan.DoseGrid.resolution(2);
    imgCT_RTSTRUCT.dpcz = Plan.CTinfo.ImagePositionPatient(3) + [0:Plan.DoseGrid.size(3)-1]' * Plan.DoseGrid.resolution(3);
        
    Plan = CreateScanningGrid(Plan,imgCT_RTSTRUCT,TVonCT);
    
    
    % Set parameters for optimization
    [Plan,ROI,warm_start_in] = SetOptParameters(OptConfig,Plan,ROI);
    
    
    % Calculate beamlets or read saved beamlets from previous run   
    if ~isfield(Plan,'PlanExistentFile')
        [warm_start_in,Plan] = ComputePijsMatrix(Plan, handles);   
    end
    [Plan] = ReadPijsMatrix(Plan, OptConfig, handles); 
     
    
    % Compute optimal spots weights with IPOPT and save in DICOM plan
    [Plan] = SpotWeightsOptimization(ROI,Plan,OptConfig,warm_start_in);
    
    
    % Compute final dose distribution with MCsquare
    [handles] = ComputeFinalDose(Plan,handles);
    
    
    % compute DVH on contours of interest
    handles.dvhs = dose_volume_histograms(handles,show_dvh,{'FinalDose'},selected_ROIs);
end
%--------------------------------------------------------------------------

function config = manageData(config, handles)
       
    % Convert yaml-read cell to mat
    if(length(config.Plan.Beams) == 1)
        config.Plan.Beams = {config.Plan.Beams};
    end
    config.Plan.Beams = cell2mat(config.Plan.Beams);
    for i = 1:length(config.Plan.Beams)
        config.Plan.Beams(i).TargetMargin = cell2mat(config.Plan.Beams(i).TargetMargin);
    end
    
    if(length(config.Plan.optFunction) == 1)
        config.Plan.optFunction = {config.Plan.optFunction};
    end
    config.Plan.optFunction = cell2mat(config.Plan.optFunction);
    
    if(length(config.Plan.SystSetUpError) == 1)
        config.Plan.SystSetUpError = {config.Plan.SystSetUpError};
    end
    config.Plan.SystSetUpError = cell2mat(config.Plan.SystSetUpError);
    
    nr_randomErr = size(config.Plan.RandSetUpError);
    for i = 1:nr_randomErr(1)
        tempVec = [];
        for j = 1:nr_randomErr(2)
           tempVec = [config.Plan.RandSetUpError{i,j} tempVec];
        end
        tempCell{i} = tempVec;
    end
    config.Plan.RandSetUpError = tempCell.';
    
    if(length(config.Plan.Opt4D) == 1)
        config.Plan.Opt4D = {config.Plan.Opt4D};
    end
    config.Plan.Opt4D = cell2mat(config.Plan.Opt4D);
    
    config.OptConfig.mixedOptimization.RandSetUpError = cell2mat(config.OptConfig.mixedOptimization.RandSetUpError);
    config.Outputs.DVH_roiSet = cell2mat(config.Outputs.DVH_roiSet);
    config.Rtest.RandSetUpError = cell2mat(config.Rtest.RandSetUpError);
    config.Rtest.SystSetUpError = cell2mat(config.Rtest.SystSetUpError);
end
%--------------------------------------------------------------------------

function config = manageOuput(config, handles)

    % Handle miropt output location
    if (~isfield(config.Plan, 'output_path') && ~isfield(config.Plan, 'PlanExistentFile'))
        default_dir = fullfile(handles.dataPath,['miropt_' config.Plan.name]);
        if (exist(default_dir, 'dir') == 7)
            try
                rmdir(default_dir,'s');
                pause(0.5);
            catch
            end
        end
        mkdir(default_dir);
        config.Plan.output_path = default_dir;    
    elseif isfield(config.Plan, 'output_path') && ~isfield(config.Plan, 'PlanExistentFile')
        if (exist(config.Plan.output_path, 'dir') == 7)
            try
                rmdir(config.Plan.output_path, 's');
                pause(0.5);
            catch
            end
        end
        mkdir(config.Plan.output_path);
    else
        config.Plan.output_path = config.Plan.PlanExistentFile;
    end
end
%--------------------------------------------------------------------------