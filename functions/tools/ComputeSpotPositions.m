%% ComputeSpotPositions
% This function computes the spots positions for all energy layers used to 
% cover the target volume with a given proton beam. For each layer, the spots
% are placed following an constant (and regular) hexagonal grid where the lateral 
% distance between spots (side length of the hexagon) is defined by the
% user. From this hexagonal grid, which initially covers the entire CT, only
% the points lying inside the selected volume to place the spots are kept. 
% The ensemble of these points constitutes the spot positions, which are
% defined by two components, representing the distance in mm from the beam
% isocenter in Beam Eye View (BEV) coordinate system (perpendicular to the 
% beam direction):
%
%    yBEV  ^ 
%          |
%          |  
%    ISO   O_______> xBEV
% 
% To encode these positions, the spots are ordered with increasing yBEV firstly and
% increasing xBEV secondly: 
%
%      x(3)    x(6)
% 
%
%  x(1)    x(4)    x(7)
%
%
%      x(2)    x(5)
%
%%
%%

%% Syntax
% |[ Layers,SystSetUpScenario,shift_BEV, gridShift_BEV,virtualSpots,nominalSpots ] = ComputeSpotPositions( Beam,SystSetUpError,imgCT_RTSTRUCT,LayerEnergy,WEPL,virtualSpots,nominalSpots,SystSetUpScenario )|

%% Description
% |[ Layers,SystSetUpScenario,shift_BEV, gridShift_BEV,virtualSpots,nominalSpots ] = ComputeSpotPositions( Beam,SystSetUpError,imgCT_RTSTRUCT,LayerEnergy,WEPL,virtualSpots,nominalSpots,SystSetUpScenario )| 
% returns the spots positions (encoded in the structure |Layers|) for each 
% energy layer in the proton beam (|Beam|). 
% If a systematic error (|SystSetUpError|) is introduced, some extra spots
% (|virtualBeamlets|) need to be added in the nominal scanning grid
% (|nominalSpots|). Systematic setup errors are simulated by shifting the
% spot weights to adjacent positions, using the nearest neighbors spots.
% The values for the systematic setup errors introduced by the user in
% DICOM coordinates (|SystSetUpError|) are trasnformed into BEV system, in
% order to know the corresponding shift that needs to be applied in the
% spot plane (|shift_BEV|). In most cases, the performed shift
% (|gridShift_BEV|) will be an approximation of the read one (|shift_BEV|).
% Therefore, the extra (|virtual|) spots serve to have a finner scanning grid
% which provides intermediate spot positions to perform this shift as
% accurate as possible.


%% Input arguments
% |Beam| - _struct_ - Structure containing the parameters for a given proton beam. The following data must be present in the structure:
%
% * |Beam.SpotSpacing| - _scalar_ - User-defined spacing between spots (in mm).
% * |Beam.Isocenter| - _array_ - Column (3x1) vector containing the DICOM coordinates of the beam isocenter, usually defined as the center of the target volume.
% * |Beam.GantryAngle| - _scalar_ - Gantry angle for current beam (in degrees).
%
% |SystSetUpError| - _array_ - Row (1x6) vector with the setup error values for x,y and z DICOM coordinates in the positive and negative directions:[x -x y -y z -z] (in mm).
%
% |imgCT_RTSTRUCT| - _struct_ - Structure containing the CT matrix and the related information. The following data must be present in the structure:
%
% * |imgCT_RTSTRUCT.dpcx| - _array_ - Column vector containing the x-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dpcy| - _array_ - Column vector containing the y-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dpcz| - _array_ - Column vector containing the z-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dspa| - _array_ - Column (3x1) vector containing the resolution of the CT for the x, y and z DICOM coordinates.
%
% |LayerEnergy| - _array_ - Row vector of (1xN) size, containing the values
% of the N energy layers (in MeV) for the current beam.
%
% |WEPL| - _array_ - 3D matrix containing the water equivalent path length
% for each voxel in the volume used to place the spots.
%
% |virtualSpots| - _scalar_ - Number of total spots to be simulated,
% including the virtual (extra) spots that help to simulate accurately the
% systematic setup errors. In the first call to this function, this
% variable is equal to zero, otherwise, it will indicate the number of
% virtual spots corresponding to the beams processed so far. 
% 
% |nominalSpots| - _scalar_ - Number of real spots to be delivered, which
% corresponds to the spot positions in the nominal case, where no errors
% are present. In the first call to this function, this variable is equal
% to zero, otherwise, it will indicate the number of nominal spots
% corresponding to the beams processed so far.
%
% |SystSetUpScenario| - _struct_ - Structure containing the index for the 
% spots (from the virtual set) that need to be considered in each scenario
% used to simulate the systematic errors, i.e., |SystSetUpScenario(s).wID|
% contains the indices of the spots that need to be used in scenario |s|.
% At this stage the input structure is empty: |SystSetUpScenario(s).wID = []|.



%% Output arguments
% |Layers| - _struct_ - Structure containing as many entries as the number of energy layers. For each layer i, the following fields must be present:
%
% * |Layers(i).Energy| - _scalar_ - Energy (in MeV) of the current layer.
% * |Layers(i).realSpotPositions| - _array_ - Matrix with size (Nx2) containing the positions of the real (nominal) spots for the current layer, i.e., the ones to be delivered. The first and second columns represents the xBEV and yBEV coords., respectively.
% * |Layers(i).virtualSpotPositions| - _array_ - Matrix with size (Mx2) containing the positions of the nominal plus virtual spots for the current layer, i.e., all spots to be computed, including the intermediate ones that serve to simulate the setup errors. The first and second columns represents the xBEV and yBEV coords., respectively.
%
% |SystSetUpScenario| - _struct_ - Structure containing the indices for the 
% spots (from the virtual set) that need to be considered in each scenario
% used to simulate the systematic errors, i.e., |SystSetUpScenario(s).wID|
% contains the indices of the spots that need to be used in scenario |s|.
%
% |shift_BEV| - _array_ - Matrix of size (6x2) containing the real shifts
% that should be performed to simulate the systematic setup errors
% introduced by the user. The first column corresponds to xBEV, while the
% second column corresponds to the yBEV coords. The six row entries are the
% values for the positive and negative directions of the x, y, and z DICOM
% axis. 
%
% |gridShift_BEV| - _array_ - Matrix of size (6x2) containing the
% approximated shifts that will be performed to simulate the systematic
% setup errors. It will be equal or proportional to the spot spacing in the
% scanning grid. The first column corresponds to xBEV, while the
% second column corresponds to the yBEV coords. The six row entries are the
% values for the positive and negative directions of the x, y, and z DICOM
% axis. 
%
% |virtualSpots| - _scalar_ - Number of total spots to be simulated,
% including the virtual (extra) spots that help to simulate accurately the
% systematic setup errors. It indicates the number of virtual spots
% corresponding to the beams processed so far.
%
% |nominalSpots|- _scalar_ - Number of real spots to be delivered, which
% corresponds to the spot positions in the nominal case, where no errors
% are present. It indicates the number of nominal spots corresponding to
% the beams processed so far.

%% NOTE
% Note that so far couch angles different from zero are not supported.

%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu


function [ Layers,SystSetUpScenario,shift_BEV, gridShift_BEV,virtualSpots,nominalSpots ] = ComputeSpotPositions( Beam,SystSetUpError,imgCT_RTSTRUCT,LayerEnergy,WEPL,virtualSpots,nominalSpots,SystSetUpScenario )

% create hexagonal grid to place the spots
HexagonalGrid = create2DHexagonalGrid(Beam.SpotSpacing,imgCT_RTSTRUCT,Beam.Isocenter);

% compute shifts for systematic setup errors (if existent) real and
% approximations according to spot grid voxel size
[shift_BEV, gridShift_BEV] = GetBEVShifts (Beam.GantryAngle,SystSetUpError,HexagonalGrid);

% Transform WEPL to energy (MeV)
WEPLtoMeV = range_to_energy(WEPL/10); % WEPL need to be in cm to give MeV
% Rotate to BEV
WEPLtoMeV_BEV = DICOMtoBEV(Beam.GantryAngle*(pi/180),imgCT_RTSTRUCT,Beam.Isocenter, WEPLtoMeV); % outputs DICOM layout (no need of inversing X and Y)

    % calculate spot positions for each layer and each shifted scenario
    
    % loop over all energy layers
    ee = 0;
    for e = 1:numel(LayerEnergy)
        
        % Create a binary mask for those voxels within the same energy interval
        if e == numel(LayerEnergy) % last layer
            maskLayer = (LayerEnergy(e) <= WEPLtoMeV_BEV)&(LayerEnergy(e-1) >= WEPLtoMeV_BEV );
        else
            maskLayer = (LayerEnergy(e+1) <= WEPLtoMeV_BEV)&(LayerEnergy(e) >= WEPLtoMeV_BEV );
        end
        
        % Create layer projection (sum along y_DICOM coord.)
        lp = squeeze(sum(maskLayer,1));
        lp = double(lp > 0); % convert to binary mask

        % Resample to SpotGrid resolution using interp2 to find spot positions in the NOMINAL case
        
        [Xq,Yq]=meshgrid(HexagonalGrid.yBEVcoords,HexagonalGrid.xBEVcoords);
        [X,Y]=meshgrid(imgCT_RTSTRUCT.dpcz'- Beam.Isocenter(3),imgCT_RTSTRUCT.dpcx'- Beam.Isocenter(1));
        
        lp_resampled_SpotGrid = interp2(X,Y,lp,Xq,Yq,'nearest');
                
        % rotation from xBEV,yBEV to yBEV,xBEV (Matlab format)
        lp_resampled_SpotGrid = permute(lp_resampled_SpotGrid,[2,1]);
        
        % Get hexagonal pattern for spots in layer
        nominalLayerMask = (lp_resampled_SpotGrid>0).*HexagonalGrid.mask;
                
        % Get spot index
        [row_lp_hex,col_lp_hex] = find(nominalLayerMask);
        % BEV coords for nominal case (spot positions)
        nominalSpotPosition_yBEV  = HexagonalGrid.yBEVcoords(row_lp_hex);
        nominalSpotPosition_xBEV  = HexagonalGrid.xBEVcoords(col_lp_hex);
        
      
        if (nnz(SystSetUpError) > 0)
            
            % Get virtual spot positions
            [virtualLayerMask,SystSetUpScenario] = virtualBeamlets(HexagonalGrid,nominalLayerMask,...
                shift_BEV,virtualSpots,SystSetUpError,SystSetUpScenario);
            % index for virtual mask
            [row,col] = find(virtualLayerMask);
            % coords for layer projection
            virtualSpotPosition_yBEV  = HexagonalGrid.yBEVcoords(row);
            virtualSpotPosition_xBEV  = HexagonalGrid.xBEVcoords(col);
            
        else
            virtualLayerMask = nominalLayerMask;
            SystSetUpScenario(1).wID = [SystSetUpScenario(1).wID;   [1:nnz(nominalLayerMask)]' + nominalSpots];
            virtualSpotPosition_xBEV = nominalSpotPosition_xBEV;
            virtualSpotPosition_yBEV = nominalSpotPosition_yBEV;
            
        end

        
        % Fill fields
        if nnz(nominalLayerMask) ~= 0
            ee = ee + 1;
            Layers(ee).Energy = LayerEnergy(e);
            Layers(ee).realSpotPositions = [ nominalSpotPosition_xBEV'  nominalSpotPosition_yBEV' ];
            Layers(ee).virtualSpotPositions = [ virtualSpotPosition_xBEV'  virtualSpotPosition_yBEV' ];            
        else
            disp(['Removing ',num2str(e),'th layer with zero spots at ',num2str(LayerEnergy(e)), ' MeV']);

        end
        
        
        virtualSpots = virtualSpots + nnz(virtualLayerMask);
        nominalSpots = nominalSpots + nnz(nominalLayerMask);
        
        
    end
    
end



