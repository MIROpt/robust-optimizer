%% ComputeIsocenter
% This function computes the the beam isocenter as the geometric center of
% the target volume. 
%%

%% Syntax
% |[Isocenter] = ComputeIsocenter(TVonCT,CT_xcoords,CT_ycoords,CT_zcoords)|

%% Description
% |[Isocenter] = ComputeIsocenter(TVonCT,CT_xcoords,CT_ycoords,CT_zcoords)|
% computes the geometric center of the target volume with binary mask |TVonCT|,
% for a CT image which DICOM coordinates for the center of each voxel are
% stored in the vectors |CT_xcoords|, |CT_ycoords|, and |CT_zcoords|, for
% x, y, and z axis, respectively.

%% Input arguments
% |TVonCT| - _array_ - 3D binary mask for the target volume (TV), i. e., voxels inside the target are equal to one, while those outside are equal to zero.
%
% |CT_xcoords| - _array_ - Column vector containing the x-DICOM coordinate for the center of each voxel of the CT (in mm).
%
% |CT_ycoords| - _array_ - Column vector containing the y-DICOM coordinate for the center of each voxel of the CT (in mm).
%
% |CT_zcoords| - _array_ - Column vector containing the z-DICOM coordinate for the center of each voxel of the CT (in mm).


%% Output arguments
% |Isocenter| - _array_ - Column (3x1) vector containing the DICOM coordinates
% for the center of the target volume. 

%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu

function [ Isocenter ] = ComputeIsocenter( TVonCT , CT_xcoords,CT_ycoords,CT_zcoords)

% inputs: 
% TVonCT = 3-dimensional binary mask for the target volume
% CT_xcoords, CT_ycoords, CT_zcoords = coordinates (DICOM format, mm) for
% each voxel on the CT image.

[x, y, z] = ind2sub(size(TVonCT), find(TVonCT));
Isocenter = [mean(CT_xcoords(x)); mean(CT_ycoords(y)); mean(CT_zcoords(z))];

end

