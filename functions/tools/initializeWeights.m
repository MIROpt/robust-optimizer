function w0 = initializeWeights(Beams, Dref)

% This function calculates analytically the spot weights to have a SOBP of
% dose Dref in a water phantom, according to the formula published by
% Bortfeld and Schlegel 1996.
% R corresponds to the postion in depth of the energy layers in a certain
% beam.


% Should we divide the total dose by the number of beams? to have an
% estimation of the dose delivered by each beam??

% Convert Gy to MeV/g (see paper from D.Jette and W.Chen 2011, PMB
Dref = Dref * 1.60217657e10;

for j=1:length(Beams)
    Energy = zeros(length(Beams(j).Layers),1);
    for l = 1:length(Beams(j).Layers)
        Energy(l) = Beams(j).Layers(l).Energy;
    end
    
    R = energy_to_range(Energy); % convert energy (MeV) to range (cm) for all the layers in a beam
    
    density = 1; % water 1g/cm3
    
    SOBP_width = (R(1) - R(end)/R(1))*100;
    Emax = Energy(1);
    
    % Calculate p value as a function of maximum energy and width of SOBP
    % (with of the tumor in the direction of the beam, expressed in % of max range) 
    % Method from (see paper from D.Jette and W.Chen 2011, PMB)
    
    SOBPW = [15 20 25 30 35 40];
    EMAX = [50 100 150 200 250];
    pvalues = [1.48   1.45   1.43   1.43   1.42   1.41
               1.46   1.43   1.42   1.41   1.40   1.38
               1.43   1.40   1.39   1.37   1.36   1.35
               1.40   1.37   1.34   1.33   1.32   1.30
               1.34   1.32   1.29   1.27   1.26   1.24];
           
    % find the closest value for SOBPW and EMAX
    [~, idx] = min(abs(Emax - EMAX));
    [~, idy]  = min(abs(SOBP_width - SOBPW));
    p = pvalues(idx,idy);
    %OJO we get complex numbers if we use p small! for MIROpt structure p
    %estimated was 1.35

    %p = 1.77; 
    alpha = 0.0022; % value from Bortfeld 1997, re-used in Jette and Chen 2011
    delta = Beams(j).LayerSpacing/10; % to convert to cm
    
    
    % wlayer has units of particle fluence, i.e., cm-2
    wlayers = zeros(length(R),1); % length(R) = number of layers
    
    R = flipdim(R,1);
    c = ( density * Dref * (p^2) * (alpha^(1/p)) * sin(pi/p) ) / (pi*(p-1));
    
    wlayers(end)       = c * ( (delta/2)^(1-(1/p)) );
    wlayers(1:(end-1)) = c * ( ( R(end) - R(1:(end-1)) + delta/2 ).^(1-(1/p)) - ( R(end) - R(1:(end-1)) - delta/2 ).^(1-(1/p)) );
    
    
    % Convert number of protons to MU (monitor units)
    
    % /* http://thread.gmane.org/gmane.comp.science.opengate.user/1564/focus=1572
    %
    % A MU is defined as a number of nC collected in the Ionization Chamber
    % (IC), which is filled with air.
    % SP corresponds to proton stopping power in air and it is based on a fit
    % from ICRU data.
    % K is a constant which depends on the mean energy loss (W) to create an
    % electron/hole pair.
    % PTP are the temperature and pression corrections.
    % Using all these parameters correctly allows for MU to absolute number of
    % protons conversion.
    %
    % 1 eV = 1,60217657 × 10-19 joule
    %
    % */
    
    K=34.23;
    SP=9.6139E-09*(Energy.^4)-7.0508E-06*(Energy.^3)+2.0028E-03*(Energy.^2)-2.7615E-01*(Energy.^1)+2.0082E+01*(Energy.^0);
    PTP=1;
    %Gain=3./(K*SP*PTP*1.602176E-10);
    Gain = (3E-9*K)/(SP*1.20479E-03*PTP*1E6*1.602176E-19);
    
    wlayerMU = wlayers./Gain';

    w0 =[];
    for i = 1:length(R)
        w0 = [w0; (Beams(j).Layers(i).SpotWeights * wlayerMU(i))];
    end

    %size(w0)
end

w0 = w0'; %OJO units? the values are very small ...
    


    %w0 = wlayer;

% to calculate w0 we need to apply the weight for each layer to all the
% spots in the same layer and save them in a 1D vector.






     