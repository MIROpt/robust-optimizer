function [] = createBoxExternalContour(new_ROIname)
%CreateBoxExternalContour creates a box which contains the whole CT
%Example : createBoxExternalContour('box')

addpath(genpath('../../../lib'));

% load CT

disp('Load CT image + RTSTRUCT.');
ttl = 'Create ROI contour';

while 1
    CT_path = uigetdir('*','Load CT file(s)');
    
    % if cancel pressed, give up
    if isequal(CT_path,0), return; end
    
    % read CT files & RTSTRUCT
    imgCT_RTSTRUCT = open_drt_2(CT_path,1); % same as open_drt but modified to avoid some dialog box
    if isempty(imgCT_RTSTRUCT)
        disp('Canceled: exit');
        return
    end
    
    datasets = struct('index',1,'array',imgCT_RTSTRUCT);
    
    % break loop if everything all right
    if strcmpi(imgCT_RTSTRUCT.dmod,'ct')
        break
    else
        messagebox('title',ttl,'text','DICOM image modality is not CT; please retry.');
    end
end

ROImask = zeros(size(imgCT_RTSTRUCT.dval));
ROImask(5:end-5,5:end-5,5:end-5) = 1;
sCT = size(imgCT_RTSTRUCT.dval);
temp = find(squeeze(sum(sum(ROImask,1),2)));
klb = max(     1,min(temp));
kub = min(sCT(3),max(temp));

% Append dilated contour to RTSTRUCT

% determine contours
crnX = imgCT_RTSTRUCT.dspa(1)*((1:sCT(1)) - (sCT(1)+1)/2);
crnY = imgCT_RTSTRUCT.dspa(2)*((1:sCT(2)) - (sCT(2)+1)/2);
tgt_line = newline([sCT(1),sCT(2),sCT(3),1]);
tgt_line.name = new_ROIname;
%tgt_line.cval = [0,1,0];
tgt_line = cntr(tgt_line,ROImask,crnX,crnY,klb,kub,0.5);
datasets.array(1).lines.array(1+length(datasets.array(1).lines.array)) = tgt_line;

% get file specs
[filename, filepath] = uiputfile({'*.dcm','DICOM-RT file (*.dcm)'},'Select file to save RTSTRUCT');
if isequal(filename,0) || isequal(filepath,0)
    % cancel
    try close(99); catch msg, disp(msg); end
    cd(path1);
    return
elseif 4<=length(filename)
    if ~strcmp('.dcm',filename(end-3:end)), filename = [filename,'.dcm']; end
else
    filename = [filename,'.dcm'];
end

% create DICOM RTSTRUCT structure and save it
wb = waitbar(0,'Writing RTSTRUCT file to disk... Please wait.');
info = save_drt(datasets,1);
dicomwrite([],convfilename(fullfile(filepath,filename)),info,'CreateMode','copy');
try close(wb); catch msg, disp(msg); end

end

function tgt_line = cntr(tgt_line,tgt_data,crnX,crnY,klb,kub,thr)
% simple contour drawing

for k = klb:kub
    tmp = tgt_data(:,:,k)';
    cmat = contourc(crnX,crnY,tmp,thr*[1,1]);
    if size(cmat,2)>1
        gons = {};
        ccol = 1;
        while cmat(1,ccol)==thr
            gons{length(gons)+1} = (cmat(:,ccol+[1:cmat(2,ccol),1]))';
            ccol = ccol + cmat(2,ccol) + 1;
            if size(cmat,2)<ccol, break; end
        end
        
        % store
        for ridx = 1:length(gons)
            tgt_line.array{k,ridx} = gons{ridx};
        end
    else
        % store
        tgt_line.array{k,1} = [];
    end
end
end








