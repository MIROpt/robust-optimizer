%% virtualBeamlets
% Systematic setup errors are simulated by shifting the spot weights to
% adjacent positions, using the nearest neighbors spots. Therefore, extra
% spot (or beamlets) need to be computed to allow the simulation of the
% setup errors following this strategy. This functions computed this extra
% or virtual spot positions for a given energy layer.
%%
%
%% Syntax
% |[ virtualLayerMask, SystSetUpScenario ] = virtualBeamlets( HexagonalGrid, nominalLayerMask, shift_BEV, virtualSpots, SystSetUpError, SystSetUpScenario )|
%
%% Description
% |[ virtualLayerMask, SystSetUpScenario ] = virtualBeamlets(
% HexagonalGrid, nominalLayerMask, shift_BEV, virtualSpots, SystSetUpError,
% SystSetUpScenario )| returns the two-dimensional array
% (|virtualLayerMask|) containing the all spots positions (including
% virtual and nominal) and the indices for the spots (from the virtual set)
% that needs to be used in each scenario of setup error
% (|SystSetUpScenario(s).wID|, being wID the index and s the scenario
% number). In order to be able to compute this data, the function uses the
% hexagonal grid covering the whole CT (|HexagonalGrid|), the binary mask
% containing the nominal spots positions (|nominalLayerMask|), the
% systematic setup error (|SystSetUpError|) and the corresponding shift to
% be applied to the spots in Beam Eye View system (|shift_BEV|).
%
%% Input arguments
% |HexagonalGrid| - _struct_ - Structure containing the information about
% the hexagonal grid. The following data is present in the structure:
%
% * |voxelSize| - _array_ - Column vector (2x1) containing the voxel size for the matrix (rectangular grid) containing the hexagonal binary mask. The first element corresponds to the vertical side of the voxel, while the second element corresponds to the horizontal side. An illustration  is provided below.
% * |yBEVcoords| - _array_ - Row vector containing the y coords in Beam Eye View system (BEV, perpendicular to the beam direction) for the center of each voxel in the 2D rectangular grid.
% * |xBEVcoords| - _array_ - Row vector containing the x coords in Beam Eye View system (BEV, perpendicular to the beam direction) for the center of each voxel in the 2D rectangular grid.
% * |mask| - _array_ - Binary mask (i.e, ones at each vertex of the hexagones and zeros otherwise) defining the hexagonal pattern where the spots must be placed.
%
%
%
%    yBEV  ^ 
%          |    0  1  0  1  0 |
%          |                  | voxelSize(1)  
%          |    1  0  1  0  1 | 
%          |
%          |    0  1  0  1  0
%          |             ____
%          |             voxelSize(2)
%          |  
%    ISO   O___________________> xBEV
%
%
% |nominalLayerMask| - _array_ - Binary two-dimensional mask (i.e, ones at
% each spot position and zeros otherwise) defining the nominal spot
% positions within the hexagonal grid for a given energy layer.
%
%
% |shift_BEV| - _array_ - Matrix of size (6x2) containing the real shifts
% that should be performed to simulate the systematic setup errors
% introduced by the user. The first column corresponds to xBEV, while the
% second column corresponds to the yBEV coords. The six row entries are the
% values for the positive and negative directions of the x, y, and z DICOM
% axis. 
%
%
% |virtualSpots|- _scalar_ - Number of total spots to be simulated,
% including the virtual (extra) spots that help to simulate accurately the
% systematic setup errors. It indicates the number of virtual spots
% corresponding to the energy layers processed so far. It is used to
% associate the right spot index, including all previous spots that has
% been already computed.
%
%
% |SystSetUpError| - _array_ - Row (1x6) vector with the setup error values
% for x,y and z DICOM coordinates in the positive and negative directions:
% [x -x y -y z -z] (in mm).
%
%
% |SystSetUpScenario| - _struct_ - Structure containing the index for the 
% spots (from the virtual set) that need to be considered in each scenario
% used to simulate the systematic errors, i.e., |SystSetUpScenario(s).wID|
% contains the indices of the spots that need to be used in scenario |s|.
% At this stage the input structure is empty: |SystSetUpScenario(s).wID = []|.
%
%% Output arguments
% |virtualLayerMask| - _array_ - Binary two-dimensional mask (i.e, ones at
% each spot position and zeros otherwise) defining the virtual spot
% positions within the hexagonal grid for a given energy layer.
%
%
% |SystSetUpScenario| - _struct_ - Structure containing the indices for the 
% spots (from the virtual set) that need to be considered in each scenario
% used to simulate the systematic errors, i.e., |SystSetUpScenario(s).wID|
% contains the indices of the spots that need to be used in scenario |s|.
%
%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu

% ------------------------------------- %

function [ virtualLayerMask, SystSetUpScenario] = virtualBeamlets( HexagonalGrid,nominalLayerMask,shift_BEV,virtualSpots,SystSetUpError,SystSetUpScenario )

% Compute systematic setup scenarios to get virtual spot positions
virtualLayerMask = nominalLayerMask ; % sum of all the nominal + virtual spot postions for each scenario

[row_lp_hex,col_lp_hex] = find(nominalLayerMask);

for s = 1:numel(SystSetUpError)
    
    if (SystSetUpError(s) ~= 0)
        
        % initialize to zero
        currentScenarioMask = zeros(size(nominalLayerMask));
        % convert shift to number of pixels in spotGrid
        shift_xBEV = round(shift_BEV(s,1) / HexagonalGrid.voxelSize(2));
        shift_yBEV = round(shift_BEV(s,2) / HexagonalGrid.voxelSize(1));
        
        % calculate voxel indices for shifted positions
        id = sub2ind(size(currentScenarioMask), row_lp_hex + shift_yBEV, col_lp_hex + shift_xBEV);
        % shift nominal mask
        currentScenarioMask(id) = 1;
        
        % add up masks
        virtualLayerMask = virtualLayerMask + currentScenarioMask;
    end
end


% create spotID, matrix with spot indices (order by increasing yBEV first and increasing xBEV secondly)
spotID = zeros(size(HexagonalGrid.mask));
spotID(logical(virtualLayerMask)) = 1:nnz(virtualLayerMask);
spotID = flipdim(spotID,1); % to have increasing yBEV upwards

% Calculate nearest neighbors interpolation to find the spot positions for each scenario
[X,Y] = meshgrid(HexagonalGrid.xBEVcoords,HexagonalGrid.yBEVcoords);

% Get spot ID for NOMINAL CASE
% w vector for current layer(nominal case)
nominalMask_flip = flipdim(nominalLayerMask,1);
nominalSpotsID = spotID.*nominalMask_flip;
t = flipdim(nominalSpotsID,1); % to save spots with increasing index
wID_currentLayer = t(logical(t>0));
% total w vector (nominal case) (contains indices for all beams!)
SystSetUpScenario(1).wID = [SystSetUpScenario(1).wID;   wID_currentLayer + virtualSpots];

for s = 1:numel(SystSetUpError)
    
    % initialize wID
    [Xq,Yq] = meshgrid(HexagonalGrid.xBEVcoords + shift_BEV(s,1),...
        HexagonalGrid.yBEVcoords - shift_BEV(s,2));
    
    shiftedMask = interp2(X,Y,nominalMask_flip, Xq,Yq,'nearest',0);
    
    % w vector for current layer (scenarios)
    virtualSpotsID = (shiftedMask > 0) .* spotID;
    t = flipdim(virtualSpotsID,1); % to get increasing ID values
    
    %check that the number of shifted spots are the same
    if (nnz(nominalSpotsID) ~= nnz(virtualSpotsID))
        
        disp('The number of spots in the current scenario differs from the number in nominal case')
        disp('Notify this error to ana.barragan@uclouvain.be');
        
        break
        
    end
    
    wID_currentLayer = t(logical(t>0));
    
    % total w vector
    SystSetUpScenario(s+1).wID = [SystSetUpScenario(s+1).wID;   wID_currentLayer + virtualSpots];
end


end

