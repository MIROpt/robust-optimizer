%% create2DHexagonalGrid
% This function creates a regular two-dimensional hexagonal grid at the
% perpendicular plane of the proton beam, which covers entire CT. The side
% lenght of the hexagon is determined by the user-defined spot spacing
% parameter. This grid is used to place the spots following an hexagonal
% pattern.
%%

%% Syntax
% |[ HexagonalGrid ] = create2DHexagonalGrid( SpotSpacing,imgCT_RTSTRUCT,Isocenter)|

%% Description
% |[ HexagonalGrid ] = create2DHexagonalGrid( SpotSpacing,imgCT_RTSTRUCT,Isocenter)|
% creates a two dimensional hexagonal grid (|HexagonalGrid|) used to place 
% the spots. The side of the hexagon is determined by the user-defined
% |SpotSpacing| (in mm). The origin of the grid is considered to be at the
% isocenter of the beam |Isocenter|.

%% Input arguments
% |SpotSpacing| - _scalar_ - User-defined spacing between spots (in mm).
%
% |imgCT_RTSTRUCT| - _struct_ - Structure containing the CT matrix and the
% related information. The following data must be present in the structure:
%
% * |imgCT_RTSTRUCT.dpcx| - _array_ - Column vector containing the x-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dpcz| - _array_ - Column vector containing the z-DICOM coordinate for the center of each voxel of the CT (in mm).
%
% |Isocenter| - _array_ - Column (3x1) vector containing the coordinates
% for the isocenter of the proton beam, in DICOM system. 

%% Output arguments
% |HexagonalGrid| - _struct_ - Structure containing the information about the hexagonal grid. The following data is present in the structure:
%
% * |voxelSize| - _array_ - Column vector (2x1) containing the voxel size for the matrix (rectangular grid) containing the hexagonal binary mask. The first element corresponds to the vertical side of the voxel, while the second element corresponds to the horizontal side. An illustration is provided below.
% * |yBEVcoords| - _array_ - Row vector containing the y coords in Beam Eye View system (BEV, perpendicular to the beam direction) for the center of each voxel in the 2D rectangular grid.
% * |xBEVcoords| - _array_ - Row vector containing the x coords in Beam Eye View system (BEV, perpendicular to the beam direction) for the center of each voxel in the 2D rectangular grid.
% * |mask| - _array_ - Binary mask (i.e, ones at each vertex of the hexagones and zeros otherwise) defining the hexagonal pattern where the spots must be placed.
%
%
%
%    yBEV  ^ 
%          |    0  1  0  1  0 |
%          |                  | voxelSize(1)  
%          |    1  0  1  0  1 | 
%          |
%          |    0  1  0  1  0
%          |             ____
%          |             voxelSize(2)
%          |  
%    ISO   O___________________> xBEV
%
%
%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu


function [ HexagonalGrid ] = create2DHexagonalGrid( SpotSpacing,imgCT_RTSTRUCT,Isocenter)
% creates an hexagonal grid with the same size of the CT to place the spots

HexagonalGrid.voxelSize = [SpotSpacing*cos(pi/6); SpotSpacing/2]; % yBEV,xBEV

HexagonalGrid.yBEVcoords = (imgCT_RTSTRUCT.dpcz(1):HexagonalGrid.voxelSize(1):imgCT_RTSTRUCT.dpcz(end)) - Isocenter(3);
HexagonalGrid.xBEVcoords = (imgCT_RTSTRUCT.dpcx(1):HexagonalGrid.voxelSize(2):imgCT_RTSTRUCT.dpcx(end)) - Isocenter(1);

HexagonalGrid.mask = zeros(numel(HexagonalGrid.yBEVcoords),numel(HexagonalGrid.xBEVcoords));
ycounter = 0;
for ny = 1:size(HexagonalGrid.yBEVcoords,2) % yBEV
    if rem(ycounter,2)~= 0
        HexagonalGrid.mask(ny,2:2:end) = 1; %odd
    else
        HexagonalGrid.mask(ny,1:2:end) = 1; %even
    end
    ycounter = ycounter + 1;
end
end

