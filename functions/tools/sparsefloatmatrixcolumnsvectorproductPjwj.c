#include <matrix.h>
#include <mex.h>
#include "stdint.h"
#include <omp.h>
#include <string.h>
#include <math.h>

/* Run as: v=sparsefloatmatrixcolumnsvectorproductPjwj(Sparse_beamlet_single,x);
 * where Sparse_beamlet_single vector has (NbrVoxels,1) dimensions and
 * x is a scalar*/

/* Compile with:  mex -largeArrayDims sparsefloatmatrixcolumnsvectorproductPjwj.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"*/

/* Definitions to keep compatibility with earlier versions of ML */
#ifndef MWSIZE_MAX
typedef int mwSize;
typedef int mwIndex;
#endif

/* undocumented function prototype */
EXTERN_C mxArray *mxCreateSparseNumericMatrix(mwSize m, mwSize n, 
    mwSize nzmax, mxClassID classid, mxComplexity ComplexFlag);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    mwIndex *ir,*jc;
    float *pr;
    mwSize i,j,idx,m,n,nnzElementsInColumnj,threadID,nnz;

    
    if (nrhs != 2) {
        mexErrMsgTxt("Usage: sparsefloatmatrixcolumnvectorproductPjwj( <sparse float matrix A>  <double vector x> . Computes A(:,j)*xj for j=1,2,..,size(A,2).");
    } else if (nlhs > 1) {
        mexErrMsgTxt("Too many output arguments.");
    } else if (! mxIsSparse(prhs[0]) || !mxIsSingle(prhs[0])) {
        mexErrMsgTxt("First argument should be a sparse float matrix.");
    } else if (! mxIsDouble(prhs[1]) || mxGetN(prhs[1]) != 1 ) {
        mexErrMsgTxt("Second argument should be an dense vector (double precision).");
    } 
    
    nnz = mxGetNzmax(prhs[0]);
    pr = mxGetData(prhs[0]); /*enables to pass single precision to mex file*/
    ir = mxGetIr(prhs[0]);
    jc = mxGetJc(prhs[0]);
    
    m  = mxGetM(prhs[0]); /*rows of sparse float matrix*/
    n  = mxGetN(prhs[0]); /*columns*/
    
    double *x;
    x = mxGetPr(prhs[1]); /*input vector*/
    
    if(mxGetM(prhs[1]) != n) {
        mexErrMsgTxt("Dimensions of A and x do not match.");
    }
    
    float *result_pr = mxCalloc(nnz, sizeof(float));

    
        #pragma omp parallel default(none) shared(result_pr,n,pr,ir,jc,x,j) private(i,idx,nnzElementsInColumnj,threadID)
    
{ 
            
   
        #pragma omp for     
        
        for(j=0; j < n; j++){
            
            idx=jc[j];
            nnzElementsInColumnj = jc[j+1]-jc[j];

            /*mexPrintf("current j\n");*/
            for(i=0; i<nnzElementsInColumnj; i++){
                result_pr[idx] = pr[idx] * (float)x[j];
                /*mexPrintf("result_pr( %d) = %f\n",idx,result_pr[idx]);*/
                idx = idx + 1;
            }
            
            
        }
        
}


 
    plhs[0] = mxCreateSparseNumericMatrix(m,n,nnz,mxSINGLE_CLASS,mxREAL); 
    
    memcpy((void*)mxGetPr(plhs[0]), (const void*)result_pr, nnz*sizeof(float));
   
    memcpy((void*)mxGetIr(plhs[0]), (const void*)ir, nnz*sizeof(mwIndex)); /*ir indices do not change*/
  
    memcpy((void*)mxGetJc(plhs[0]), (const void*)jc, (n+1)*sizeof(mwIndex));/*jc indices do not change*/
    

    return;
    
}