function [WEPL] = raytracing(GantryAngle,direction, imgCT_RTSTRUCT, maskTarget, SPR)


% This function calculates the path of a ray through a 3D space (raypath)
% and gives the indeces of the intersected voxels (x,y,z). 
% It is based on a fast and simple voxel traversal algorithm through a 3D space partition (grid)
% proposed by J. Amanatides and A. Woo (1987).
%
% Input:
%    direction (proton beam direction)
%    imgCT_RTSTRUCT (CT image and RTSTRUCT with (dilated) target information).
%    maskTarget a binary mask for the target volume
%    SPR = stopping power ratios chart for the CT image
% Output:
%    WEPL in mm 
% Author: 
%    Jesús P. Mena-Chalco. 
% Adapted by: 
%    Ana Barragan
%
 

gridCT = struct('size',[imgCT_RTSTRUCT.ddim(2),imgCT_RTSTRUCT.ddim(1),imgCT_RTSTRUCT.ddim(3)],...
                'minBound', [imgCT_RTSTRUCT.dpcy(1),imgCT_RTSTRUCT.dpcx(1),imgCT_RTSTRUCT.dpcz(1)]',...
                'maxBound', [imgCT_RTSTRUCT.dpcy(end),imgCT_RTSTRUCT.dpcx(end),imgCT_RTSTRUCT.dpcz(end)]');
            
% create matrix to store WEPLvalues            
WEPL = zeros(gridCT.size);
            
voxelSizeX = imgCT_RTSTRUCT.dspa(2);
voxelSizeY = imgCT_RTSTRUCT.dspa(1);
voxelSizeZ = imgCT_RTSTRUCT.dspa(3);

% get the corner of the voxel instead of the center
            
gridCT.minBound = [gridCT.minBound(1) - voxelSizeX/2;...
                   gridCT.minBound(2) - voxelSizeY/2;...
                   gridCT.minBound(3) - voxelSizeZ/2]; 
               
gridCT.maxBound = [gridCT.maxBound(1) + voxelSizeX/2;...
                   gridCT.maxBound(2) + voxelSizeY/2;...
                   gridCT.maxBound(3) + voxelSizeZ/2];

% starting points, voxels inside the dilatedTarget (Backwards raytracing)
% A raytracing for each voxel will be performed

[start_idx, start_idy, start_idz] = ind2sub(gridCT.size, find(maskTarget));
start_x = imgCT_RTSTRUCT.dpcy(start_idx); 
start_y = imgCT_RTSTRUCT.dpcx(start_idy); 
start_z = imgCT_RTSTRUCT.dpcz(start_idz);



%%%% PLOT %%%
% figure;
% hold on;
% 
%  view(0,-90); % to see the plot in DICOM XY plane
% % axis tight;
% xlabel('x (R-L)');
% ylabel('y (A-P)');
% zlabel('z (I-S)');
% title(['3D plot for beam at ', num2str(GantryAngle)]);
% grid on;
%%%%

boxSize = [voxelSizeX*gridCT.size(1), voxelSizeY*gridCT.size(2), voxelSizeZ*gridCT.size(3)]' ;

%%% PLOT %%%
% plot starting coord. --> center of each voxel in the dilatedTarget

% plot3(start_y, start_x, start_z, 'r.', 'MarkerSize', 15);
% quiver3(start_y(1), start_x(1), start_z(1), direction(2), direction(1), direction(3), 400);
%%%%

% calculate voxel step: +1 or -1 according to the direction of the
% ray and parameters to calculate tVoxelX,Y and Z
if (direction(1)>=0)
    a(1) = 0;
    stepX = 1;
else
    a(1) = -1/gridCT.size(1);
    stepX = -1;
end;

if (direction(2)>=0)
    a(2) = 0;
    stepY = 1;
else
    a(2) = -1/gridCT.size(2);
    stepY = -1;
end;

if (direction(3)>=0)
    a(3) = 0;
    stepZ = 1;
else
    a(3) = -1/gridCT.size(3);
    stepZ = -1;
end;


tDeltaX    = voxelSizeX/abs(direction(1));
tDeltaY    = voxelSizeY/abs(direction(2));
tDeltaZ    = voxelSizeZ/abs(direction(3));

%tic
for i = 1:length(start_idx)
    %tic
    WEPLvalue = 0;
    
    x = start_idx(i);
    y = start_idy(i);
    z = start_idz(i);
     
    xpos = start_x(i);
    ypos = start_y(i);
    zpos = start_z(i);
    
    tVoxelX = (x)/gridCT.size(1) + a(1);
    tVoxelY = (y)/gridCT.size(2) + a(2);
    tVoxelZ = (z)/gridCT.size(3) + a(3);
    
    voxelMaxX  = gridCT.minBound(1) + tVoxelX*boxSize(1);
    voxelMaxY  = gridCT.minBound(2) + tVoxelY*boxSize(2);
    voxelMaxZ  = gridCT.minBound(3) + tVoxelZ*boxSize(3);
    
    
    tMaxX = (voxelMaxX-xpos)/direction(1);
    tMaxY = (voxelMaxY-ypos)/direction(2);
    tMaxZ = (voxelMaxZ-zpos)/direction(3);
    
    %%% PLOT %%%
    %text(xpos, ypos, zpos, 'origin');
%     plot3(xpos, ypos, zpos, 'k.', 'MarkerSize', 15);
%     quiver3(xpos, ypos, zpos, direction(1), direction(2), direction(3), 400);
    %%%%%%
 

    while ( (x<=gridCT.size(1))&&(x>=1) && (y<=gridCT.size(2))&&(y>=1) && (z<=gridCT.size(3))&&(z>=1) )
        
  
        % Calculate intersection point and path inside each voxel
        tVoxelX = (x)/gridCT.size(1) + a(1);
        tVoxelY = (y)/gridCT.size(2) + a(2);
        tVoxelZ = (z)/gridCT.size(3) + a(3);
        
        voxelMaxX  = gridCT.minBound(1) + tVoxelX*boxSize(1);
        voxelMaxY  = gridCT.minBound(2) + tVoxelY*boxSize(2);
        voxelMaxZ  = gridCT.minBound(3) + tVoxelZ*boxSize(3);
        
        % distance traveled on each voxel
        dMaxX = (voxelMaxX-xpos)/direction(1);
        dMaxY = (voxelMaxY-ypos)/direction(2);
        dMaxZ = (voxelMaxZ-zpos)/direction(3);
        
        raypath = min([dMaxX,dMaxY,dMaxZ]);

        % calculate the WEPL
        WEPLvalue = WEPLvalue + raypath * SPR(x,y,z); 
        
        xpos = xpos + raypath*direction(1);
        ypos = ypos + raypath*direction(2);
        zpos = zpos + raypath*direction(3);
        
        
        %%% PLOT %%%
%         plot3(xpos, ypos, zpos, 'g.', 'MarkerSize', 15);
%       
%   
%         %fprintf('\nProton path in voxel [%d %d %d] = %f', [x y z raypath] );
%         
%         t1 = [(x-1)/gridCT.size(1), (y-1)/gridCT.size(2), (z-1)/gridCT.size(3) ]';
%         t2 = [  (x)/gridCT.size(1),  (y)/gridCT.size(2),    (z)/gridCT.size(3) ]';
%         
%         vmin = (gridCT.minBound + t1.*boxSize)';
%         vmax = (gridCT.minBound + t2.*boxSize)';
%         
%         smallBoxVertices = [vmax(1) vmin(2) vmin(3); vmax(1) vmax(2) vmin(3); vmin(1) vmax(2) vmin(3); vmin(1) vmax(2) vmax(3); vmin(1) vmin(2) vmax(3); vmax(1) vmin(2) vmax(3); vmin; vmax ];
%         smallBoxFaces    = [1 2 3 7; 1 2 8 6; 1 6 5 7; 7 5 4 3; 2 8 4 3; 8 6 5 4];
%         
%         h = patch('Vertices', smallBoxVertices, 'Faces', smallBoxFaces, 'FaceColor', 'blue', 'EdgeColor', 'white');
%         set(h,'FaceAlpha',0.2);
        
        %%%%%%
            
        % ---------------------------------------------------------- %
        % check if voxel [x,y,z] contains asize(2) intersection with the ray
        %
        %   if ( intersection )
        %       break;
        %   end;
        % ---------------------------------------------------------- %

        
        if (tMaxX < tMaxY)
            if (tMaxX < tMaxZ)
                x = x + stepX;
                tMaxX = tMaxX + tDeltaX;
            else
                z = z + stepZ;
                tMaxZ = tMaxZ + tDeltaZ;
            end;
        else
            if (tMaxY < tMaxZ)
                y = y + stepY;
                tMaxY = tMaxY + tDeltaY;
            else
                z = z + stepZ;
                tMaxZ = tMaxZ + tDeltaZ;
            end;
        end;
        
    end;
    %toc
    WEPL(start_idx(i),start_idy(i),start_idz(i)) = WEPLvalue;
end
%toc

end
