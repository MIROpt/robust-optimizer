#include <matrix.h>
#include <mex.h>
#include "stdint.h"
#include <omp.h>

/* Run as: Jj=sparsefloatmatrixvectorproduct_fderP_methodA_parallel(Sparse_beamlets_single,fder);
 * where Sparse_beamlets_single matrix has (NbrVoxels,NbrSpots) dimensions and
 * w has (NbrVoxels,1) dimensions*/

/* Compile with:  mex -largeArrayDims sparsefloatmatrixvectorproduct_fderP_methodA_parallel.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"*/

/* Definitions to keep compatibility with earlier versions of ML */
#ifndef MWSIZE_MAX
typedef int mwSize;
typedef int mwIndex;
#endif

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    mwIndex *ir,*jc;
    double *x,*J;
    float *pr;
    mwSize i,j,idx,m,n,nnz,nnzElementsInColumnj,threadID;
    
    
    int nthreads=omp_get_num_procs();
    if (nthreads > 18){
        /* When intalling MIROpt in ´monster´ (UPenn) which has 72 threads,
         * we realized that it was much slower than in marvin, 
         * that's probably due to the time for malloc for all threads 
         * is bigger than the time for the multiplication itself, 
         * that's why we use 10 threads at maximum*/
        nthreads = 10; 
    }
    omp_set_num_threads(nthreads);
    
    if (nrhs != 2) {
        mexErrMsgTxt("Usage: sparseTmatrixvector <sparse matrix A> <dense vector x> . Computes A*x.");
    } else if (nlhs > 1) {
        mexErrMsgTxt("Too many output arguments.");
    } else if (! mxIsSparse(prhs[0])) {
        mexErrMsgTxt("First argument should be a sparse float matrix.");
    } else if (mxIsSparse(prhs[1]) || mxGetN(prhs[1]) != 1) {
        mexErrMsgTxt("Second argument should be a dense vector.");
    }
    
    pr = mxGetData(prhs[0]); /*enables to pass single precision to mex file*/
    ir = mxGetIr(prhs[0]);
    jc = mxGetJc(prhs[0]);
    
    m  = mxGetM(prhs[0]); /*rows of sparse float matrix*/
    n  = mxGetN(prhs[0]); /*columns of sparse float matrix*/
    
    
    /*mexPrintf("n %d \n",n);
    mexPrintf("m %d \n",m);*/
    
    nnz = mxGetNzmax(prhs[0]);
    
    x = mxGetPr(prhs[1]); /*fder vector*/
    
    if(mxGetM(prhs[1]) != m) {
        mexErrMsgTxt("Dimensions of A and x do not match.");
    }
    
    
    plhs[0] = mxCreateDoubleMatrix(1, n, mxREAL); /*output vector --> row for scenario s in Jacobian, row vector with n elements*/
    J = mxGetPr(plhs[0]); /*output vector*/
    for(i=0; i<n; i++) {
        J[i] = 0;
    }
    

        #pragma omp parallel default(none) shared(j,J,m,n,pr,ir,x,jc) private(i,idx,threadID,nnzElementsInColumnj)
    
{
        
        /*threadID = omp_get_thread_num();*/
        /*mexPrintf("threadID %d \n",threadID);*/
        
        
        #pragma omp for      
        
        for(j=0; j < n; j++){
            /*mexPrintf("j %d for threadID %d \n",j,threadID);*/
            idx=jc[j];
            nnzElementsInColumnj = jc[j+1]-jc[j];
            /*mexPrintf("nnzElementsInColumnj %d \n",nnzElementsInColumnj);*/
        
            for(i=0; i<nnzElementsInColumnj; i++){
                J[j] =  J[j] + pr[idx] * x[ir[idx]];
                /*mexPrintf("i %d for threadID %d, idx= %d\n",i,threadID,idx);*/
                idx = idx + 1;
                
               
            }
        }
     
        
              
}
    return;
}



