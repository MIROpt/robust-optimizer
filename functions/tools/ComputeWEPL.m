%% ComputeWEPL
% This function computes the water equivalent length (WEPL) for each voxel
% in the volume of interest, for a given direction of the proton beam. 
%%

%% Syntax
% |[ WEPL ] = ComputeWEPL( ScannerDirectory,MaterialsDirectory,GantryAngle,imgCT_RTSTRUCT,RTVonCT)|

%% Description
% |[ WEPL ] = ComputeWEPL( ScannerDirectory,MaterialsDirectory,GantryAngle,imgCT_RTSTRUCT,RTVonCT)| 
% returns a three-dimensional matrix containing the WEPL for each voxel
% in the volume of interest (|RTVonCT|), for a given proton beam direction
% (GantryAngle). The function can be divided in two parts. The first part
% converts the Hounsfield Units (HU) of each voxel in the CT image (stored
% in |imgCT_RTSTRUCT.dval| into stopping power ratios (SPR), using the 
% calibration curve for the CT scan used for imaging (|ScannerDirectory|).
% The second part performs a raytracing following the direction of the proton
% beam to accumulate the SPR on each voxel, and then obtain the final WEPL. 

%% Input arguments
% |ScannerDirectory| - _string_ - Name of the CT scanner used to image the patient. This will be used to select the right CT calibration curve.
%
% |MaterialsDirectory| - _string_ - Path pointing to the location of the material list from MCsquare.
%
% |GantryAngle| - _scalar_ - Gantry angle for the b-th beam (in degrees).
% |imgCT_RTSTRUCT| - _struct_ - Structure containing the CT matrix and the related information. The following data must be present in the structure:
% 
% * |imgCT_RTSTRUCT.dval| - _array_ - 3D matrix containing the Hounsfield Units (HU) for each voxel in the CT image.
% * |imgCT_RTSTRUCT.dpcx| - _array_ - Column vector containing the x-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dpcy| - _array_ - Column vector containing the y-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dpcz| - _array_ - Column vector containing  the z-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dspa| - _array_ - Column (3x1) vector containing the resolution of the CT for the x, y and z DICOM coordinates (in mm).
% 
% |RTVonCT| - _array_ - 3D binary mask for the volume used to place the spots.

%% Output arguments
% |WEPL| - _array_ - 3D matrix containing the water equivalent path length
% for each voxel of the volume used to place the spots (|RTVonCT|)

%% NOTE
% Note that so far couch angles different from zero are not supported.

%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu


function [ WEPL ] = ComputeWEPL( ScannerDirectory,MaterialsDirectory,GantryAngle,imgCT_RTSTRUCT,RTVonCT)

% Conversion HU to sttoping power ratio (SPR)

disp('Computing SPR data for: ')
disp(['Scanner --> ', ScannerDirectory]);
disp(['Materials --> ',MaterialsDirectory]);

[HU, ~, SPRdata] = Compute_SPR_data(ScannerDirectory);
% Compute SPR (stopping power ratio)
SPR = interp1(HU, SPRdata, double(imgCT_RTSTRUCT.dval), 'linear','extrap');

% Compute WEPL (does not include couch Angle!)
%WEPL = FastWEPL_ROI(single(SPR), GantryAngle, single(imgCT_RTSTRUCT.dspa(1:3)), single(RTVonCT));

%alternative /* OLD WAY slower*/
BeamAngleRad = GantryAngle * (pi/180); % degrees to radians

% Calculate Beam direction (cosine direction)
% calculate alpha (angle between beam and yDICOM (Xmatlab))
if (BeamAngleRad <= pi)
    alpha = pi - BeamAngleRad;
else
    alpha = BeamAngleRad - pi;
end
% calculate beta (angle between beam and xDICOM (Ymatlab))
if (BeamAngleRad <= (pi/2))
    beta = pi/2 - BeamAngleRad;
elseif ( ((pi/2) < BeamAngleRad) && (BeamAngleRad <= (3*pi/2)))
    beta = BeamAngleRad - pi/2;
else
    beta = (2*pi - BeamAngleRad) + pi/2;
end

BeamDir = [cos(alpha) cos(beta) 0]; % Beam direction
RTVonCT = permute(RTVonCT,[2 1 3]);  
SPR = permute(SPR,[2 1 3]);

WEPL = raytracing(GantryAngle, BeamDir, imgCT_RTSTRUCT, RTVonCT, SPR);

WEPL = permute(WEPL,[2 1 3]);
% end alternative
end

