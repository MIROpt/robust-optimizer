%% ComputeEnergyLayers
% This function computes the energy layers (in MeV) needed to cover the
% target for a specific beam direction.  
%%

%% Syntax
% |[ LayerEnergy ] = ComputeEnergyLayers(WEPL,Beam)|

%% Description
% |[ LayerEnergy ] = ComputeEnergyLayers(WEPL,Beam)| computes the energy
% layers (in MeV) needed to cover the target along a specific beam
% direction, and stores them in the vector |LayerEnergy|. First,
% the WEPL interval (minimum and maximum values) needed to cover the target
% is are computed, to which the water equivalent thinkness (WET) of the range 
% shifter, if present,is added. Second, the WEPL interval is divided into N 
% layers  with a constant spacing given by the user-defined |Beam.LayerSpacing|.
% Then, the WEPL value for each layer is converted to energy units (MeV).
% 

%% Input arguments
% |WEPL| - _array_ - 3D matrix containing the water equivalent path length
% for each voxel in the volume used to place the spots.
%
% |Beam| - _struct_ - Structure containing the parameters for a given proton 
% beam. The following data must be present in the structure:
%
% * |Beam.NumberOfRangeShifters| - _scalar_ - Number of range shifter added to the beam, it can be equal to 1 or zero.
% * |Beam.RSinfo.RangeShifterSetting| - _string_ - Machine specific setting attribute for the range shifter. If set to 'IN', the range shifter water equivalent thinkness is taken into account to compute the layers. Otherwise, it is not taken into account.
% * |Beam.RSinfo.RangeShifterWET| - _scalar_ - Water equivalent thickness (in mm) of the range shifter at the central axis for the beam energy incident upon the device.
% * |Beam.LayerSpacing| - _scalar_ - User-defined spacing between energy layers, expressed in water equivalent thickness (in mm).

%% Output arguments
% |LayerEnergy| - _array_ - Row vector of (1xN) size, containing the values
% of the N energy layers (in MeV) for the current beam.

%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu

function [ LayerEnergy ] = ComputeEnergyLayers(WEPL,Beam )

% find WEPL max and min
maxWEPL = max(WEPL(logical(WEPL))); % mm (coord.DICOM)
minWEPL = min(WEPL(logical(WEPL)));

% Take into account air gap between nozzle and CT borders
% increment of 0.46mm in WEPL (*Kevin's data*)
maxWEPL = maxWEPL + 0.046;
minWEPL = minWEPL + 0.046;

% Add range shifter WET

if (Beam.NumberOfRangeShifters == 1)
    if strcmp(Beam.RSinfo.RangeShifterSetting,'IN')
        maxWEPL = maxWEPL + Beam.RSinfo.RangeShifterWET;
        minWEPL = minWEPL + Beam.RSinfo.RangeShifterWET;
    else % OUT
        disp('Range shifter is OUT of the beam path');
    end
elseif(Beam.NumberOfRangeShifters == 0)
    disp('No range shifter is set for this beam');
else
    error('Incorrect NumberOfRangeShifters. Only one range shifter per beam is supported');
end


% Calculate energy layers (cm-WEPL), first layer corresponds to the most energetic one.
LayerRange = maxWEPL:-Beam.LayerSpacing:(minWEPL-Beam.LayerSpacing);
LayerEnergy = range_to_energy(LayerRange/10); % depth need to be in cm to get the energy in MeV


end

