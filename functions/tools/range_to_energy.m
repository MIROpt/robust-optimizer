%% range_to_energy
% This function converts the equivalent range in water (defined as r80,
% i.e., the position of the 80% dose in the distal falloff) to incident 
% energy of the proton beam (in MeV). The formula comes from Loic Grevillot
% et al. [1, 2], from a fitting to the NIST/ICRU database.
%
% [1] L. Grevillot, et al. "A Monte Carlo pencil beam scanning model for 
% proton treatment plan simulation using GATE/GEANT4."
% Phys Med Biol, 56(16):5203–5219, Aug 2011.
% [2] L. Grevillot, et al. "Optimization of geant4 settings for proton 
% pencil beam scanning simulations using gate". Nuclear Instruments and 
% Methods in Physics Research Section B: Beam Interactions
% with Materials and Atoms, 268(20):3295 – 3305, 2010.
%%

%% Syntax
% |E0 = range_to_energy(r80)|

%% Description
% |E0 = range_to_energy(r80)| transforms the equivalent proton range in
% water into incident energy.

%% Input arguments
% |r80| - _array_ - Row vector containing the equivalent proton range in
% water (in cm)

%% Output arguments
% |E0| - _array_ - Row vector containing the corresponding incident energy values
% (in MeV).

%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu

% ----------------------------------- %

function E0 = range_to_energy(r80)

%E0 = zeros(size(r80));

E0 = exp(3.464048 + 0.561372013*log(r80) - 0.004900892*(log(r80)).^2 + 0.001684756748*(log(r80)).^3);


