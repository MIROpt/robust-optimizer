%% CreateScanningGrid 
% This functions creates a scanning grid according to the user-defined
% parameters (beam angles, spot spacing, energy layer distance, target 
% margin, ...) and calculates the spot positions to cover the target 
% volume. For each beam angle, the scanning grid is divided in energy 
% layers which are perpendicular to the beam direction. For each energy
% layer, the spot position is given by two components, representing the 
% distance in mm from the beam isocenter in Beam Eye View (BEV) coordinate
% system, which is defined as follows:
%
%    yBEV  ^ 
%          |
%          |  
%    ISO   O_______> xBEV

%%

%% Syntax
% |[ Plan ] = CreateScanningGrid( Plan, imgCT_RTSTRUCT,TVonCT )|

%% Description
% |[ Plan ] = CreateScanningGrid( Plan, imgCT_RTSTRUCT,TVonCT )| returns
% the |Plan| structure containing all the plan parameters, including the
% spots positions for a given CT (|imgCT_RTSTRUCT|) containing a target volume with
% binary mask |TVonCT|, according to the user-defined beam settings encoded
% in |Plan.Beams|. 

%% Input arguments
% |Plan| - _struct_ - MIROpt structure containing all the user-defined plan
% parameters needed to create the scanning grid. The following data must be
% present in the structure: 
%
% * |Plan.Beams(b).GantryAngle| - _scalar_ - Gantry angle for the b-th beam (in degrees).
% * |Plan.Beams(b).CouchAngle| - _scalar_ - Couch angle for the b-th beam (in degrees). 
% * |Plan.Beams(b).Isocenter| - _array_ - Column (3x1) vector containing the coordinates for the isocenter of the b-th beam, in DICOM system. If empty, the isocenter will be computed automatically as the geometric center of the target volume |TVonCT|.
% * |Plan.Beams(b).TargetMargin| - _array_ - Row (1x3) vector with the margin to be applied around the target volume before placing the spots. NOTE that this margin is only used to ensure that the target is well covered by the scanning grid. It is not related to robustness against uncertainties.
% * |Plan.ScannerDirectory| - _string_ - Name of the CT scanner used to image the patient. This will be used to select the right CT calibration curve.
% * |Plan.MaterialsDirectory| - _string_ - Path pointing to the location of the material list from MCsquare.
% * |Plan.SystSetUpError| - _array_ - Row (1x6) vector with the setup error values for x,y and z DICOM coordinates in the positive and negative directions: [x -x y -y z -z].
% * |Plan.SystSetUpScenario| - _struct_ - Structure with the spot ID and corresponding shift for each scenario simulating a given systematic setup error.
%
% |imgCT_RTSTRUCT| - _struct_ - Structure containing the CT matrix and the
% related information. The following data must be present in the structure:
%
% * |imgCT_RTSTRUCT.dpcx| - _array_ - Column vector containing the x-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dpcy| - _array_ - Column vector containing the y-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dpcz| - _array_ - Column vector containing the z-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dspa| - _array_ - Column (3x1) vector containing the resolution of the CT for the x, y and z DICOM coordinates.
% 
% |TVonCT| - _array_ - 3D binary mask for the target volume (TV), i. e.,
% voxels inside the target are equal to one, while those outside are equal
% to zero.

%% Output arguments
% |Plan| - _struct_ - MIROpt structure containing all the plan parameters,
% including the computed spot positions in the corresponding fields.

%% NOTE
% Note that so far couch angles different from zero are not supported.

%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu


function [ Plan ] = CreateScanningGrid( Plan, imgCT_RTSTRUCT,TVonCT )

Plan = countScenarios(Plan);

% Initialize spots number for nominal and robust case
virtualSpots = 0; % includes auxiliar spots to cover shifted positions
nominalSpots = 0;

% loop over all beams

SystSetUpScenario = Plan.SystSetUpScenario;

for b = 1:size(Plan.Beams,2)  
        
    disp(['Computing spot positions for beam at ',num2str(Plan.Beams(b).GantryAngle)]);
    
    % Calculate ISOCENTER
    if isempty(Plan.Beams(b).Isocenter)
        % If Beam Isocenter is empty, it will be calculated automatically as the center of the target volume
        Plan.Beams(b).Isocenter = ComputeIsocenter(TVonCT,imgCT_RTSTRUCT.dpcx,imgCT_RTSTRUCT.dpcy,imgCT_RTSTRUCT.dpcz);
    end
        
    % 1. DILATATION OF TARGET VOLUME: TV to RTV (to place spots in a volume that takes into account penumbra and  setup & range errors)
    
    % could be replaced by reggui function:
    % handles = Dilation(image,params,im_dest,handles,shape)
    RTVonCT = DilateROI(TVonCT,Plan.Beams(b).TargetMargin,imgCT_RTSTRUCT.dspa);
  
    % 2. CALCULATE ENERGY LAYERS (RANGE IN WATER) AND SPOT POSITIONS (including shift for setup error scenarios)
    
    Plan.Beams(b).CouchAngle = 0; % so far it needs to be zero

    % 2.1. Raytracing to calculate WEPL in RTV 
    WEPL = ComputeWEPL(Plan.ScannerDirectory,Plan.MaterialsDirectory,Plan.Beams(b).GantryAngle,imgCT_RTSTRUCT,RTVonCT); 
    % this not include couch angle :( , we will need to include it in the future
    
    % It could be replaced by reggui function in reggui/algorithms:
    % handles = WEPL_computation(image,params,im_dest,handles,roi), which
    % uses  HU_to_WE and includes couch angle!
    
    % 2.2. Compute energy layers (in MeV)
    [LayerEnergy] = ComputeEnergyLayers(WEPL,Plan.Beams(b));

    % 2.3. Compute spot positions (mm, in BEV coords)
    [Layers,SystSetUpScenario,Plan.Beams(b).interp_shiftBEV,...
     Plan.Beams(b).shift_BEV,virtualSpots,nominalSpots] = ComputeSpotPositions(Plan.Beams(b),...
     Plan.SystSetUpError,imgCT_RTSTRUCT,LayerEnergy,WEPL,virtualSpots,nominalSpots,SystSetUpScenario);
    
    % 2.4. Fill-in fields
    for e = 1:numel(Layers)
        Plan.Beams(b).Layers(e).Energy = Layers(e).Energy;
        Plan.Beams(b).Layers(e).nominalSpotPosition = Layers(e).realSpotPositions;
        Plan.Beams(b).Layers(e).robustSpotPositions = Layers(e).virtualSpotPositions;
        Plan.Beams(b).Layers(e).SpotPositions = Layers(e).virtualSpotPositions;
        Plan.Beams(b).Layers(e).SpotWeights = ones(size(Plan.Beams(b).Layers(e).robustSpotPositions,1),1); 
        % equal to 1 for beamlet dose calculation
    end
    

    % Clear auxiliar variables
    clearvars LayerEnergy Layers ;
  
end

Plan.virtualSpots = virtualSpots; % includes auxiliar spots to cover shifted positions
Plan.nominalSpots = nominalSpots;
Plan.SystSetUpScenario = SystSetUpScenario;

% Calculate ws (vector with spot positions in each scenario)
for s = 1:Plan.NbrSystSetUpScenarios 
    Plan.SystSetUpScenario(s).ws = zeros(Plan.virtualSpots,1);
    Plan.SystSetUpScenario(s).ws(Plan.SystSetUpScenario(s).wID) = 1; 
end
   



end