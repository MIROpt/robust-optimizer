
% matrix multiplication and handling
current_dir = pwd; % Assumes that we are in MIROpt_lib/others
% float
cd('../fastMatrixMultiplication/float')
mex -largeArrayDims sparsefloatmatrixvectorproduct_methodA_parallel.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"
mex -largeArrayDims sparsefloatmatrixvectorproduct_fderP_methodA_parallel.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"
mex -largeArrayDims sparsefloatmatrixcolumnsvectorproductPjwj.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"

%double
cd(current_dir);
cd('../fastMatrixMultiplication/double')
mex -largeArrayDims sparsedoublematrixvectorproduct_fderP_methodA_parallel.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"
mex -largeArrayDims sparsedoublematrixvectorproduct_methodA_parallel.c CFLAGS="\$CFLAGS -fopenmp" LDFLAGS="\$LDFLAGS -fopenmp"

% Beamlets and dose import/export
cd(current_dir)
cd('../import_read_beamlets_dose')
mex -largeArrayDims mexSparseBeamletsReader.c
mex -largeArrayDims mexSparseBeamletsReader_float.c
mex -largeArrayDims mexSparseBeamletsReader_float_OptROIvoxels.c
mex -largeArrayDims sparsefloatmatrix_OptROIvoxels_noparallel.c CFLAGS="\$CFLAGS -fopenmp" 

% others
cd(current_dir)
cd('../others')
mex CFLAGS='\$CFLAGS -fopenmp' LDFLAGS='\$LDFLAGS -fopenmp' FastWEPL.c
mex CFLAGS='\$CFLAGS -fopenmp' LDFLAGS='\$LDFLAGS -fopenmp' FastWEPL_ROI.c