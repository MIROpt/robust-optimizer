%% GetBEVShifts
% Systematic setup errors are simulated by shifting the spots weights to
% adjacent positions, using the nearest neighbors spots. This function
% transforms the user-defined systematic setup error in DICOM system
% (x:Right-Left, y:Anterior-Posterior, z:Inferior-Superior) into BEV system
% (Beam-Eye-View, two dimensional plane perpendicular to the beam
% direction). The shifts parallel to the beam direction are not taken into
% account. 
%%

%% Syntax
% |[ shiftBEV,interp_shiftBEV ] = GetBEVShifts( GantryAngle,SystSetUpError,HexagonalGrid )|

%% Description
% |[ shiftBEV,interp_shiftBEV ] = GetBEVShifts( GantryAngle,SystSetUpError,HexagonalGrid )|
% transforms the values for the systematic setup errors introduced by the
% user in DICOM coordinates (|SystSetUpError|) into BEV system, in order to
% know the corresponding shift that needs to be applied in the spot plane
% (|shiftBEV|), for the given beam at angle |GantryAngle|. Since the spots 
% are placed in a discrete hexagonal grid (|HesagonalGrid|), the distance 
% to the nearest spot will usually differ from the shift that needs to be
% perfomed (|shiftBEV|). In this case, the perfomed shift (|interp_shiftBEV)
% will be an approximation, being equal or proportional to the distance
% between spots in the hexagonal grid. 

%% Input arguments
% |GantryAngle| - _scalar_ - Gantry angle for current beam (in degrees).
%
% |SystSetUpError| - _array_ - Row (1x6) vector with the setup error values for x,y and z DICOM coordinates in the positive and negative directions: [x -x y -y z -z] (in mm).
%
% |HexagonalGrid| - _struct_ - Structure containing the information about the hexagonal grid used to place the spots. The following data must be present in the structure:
%
% * |voxelSize| - _array_ - Column vector (2x1) containing the voxel size for the matrix (rectangular grid) containing the hexagonal binary mask. The first element corresponds to the vertical side of the voxel, while the second element corresponds to the horizontal side. An illustration is provided below.
% * |yBEVcoords| - _array_ - Row vector containing the y coords in Beam Eye View system (BEV, perpendicular to the beam direction) for the center of each voxel in the 2D rectangular grid.
% * |xBEVcoords| - _array_ - Row vector containing the x coords in Beam Eye View system (BEV, perpendicular to the beam direction) for the center of each voxel in the 2D rectangular grid.
% * |mask| - _array_ - Binary mask (i.e, ones at each vertex of the hexagones and zeros otherwise) defining the hexagonal pattern where the spots must be placed.
%
%
%
%    yBEV  ^ 
%          |    0  1  0  1  0 |
%          |                  | voxelSize(1)  
%          |    1  0  1  0  1 | 
%          |
%          |    0  1  0  1  0
%          |             ____
%          |             voxelSize(2)
%          |  
%    ISO   O___________________> xBEV


%% Output arguments
% |shiftBEV| - _array_ - Matrix of size (6x2) containing the real shifts
% that should be performed to simulate the systematic setup errors
% introduced by the user. The first column corresponds to xBEV, while the
% second column corresponds to the yBEV coords. The six row entries are the
% values for the positive and negative directions of the x, y, and z DICOM
% axis. 
%
% |interp_shiftBEV| - _array_ - Matrix of size (6x2) containing the
% approximated shifts that will be performed to simulate the systematic
% setup errors. It will be equal or proportional to the spot spacing in the
% scanning grid. The first column corresponds to xBEV, while the
% second column corresponds to the yBEV coords. The six row entries are the
% values for the positive and negative directions of the x, y, and z DICOM
% axis. 
%
%% NOTE
% Note that so far couch angles different from zero are not supported.
%
%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu


function [ shiftBEV,interp_shiftBEV ] = GetBEVShifts( GantryAngle,SystSetUpError,HexagonalGrid )

% Find shifts (setup error scenarios) correponding to the current beam
% NOTE: we assume that CouchAngle = 0; hence we have:
% shift_xBEV(due to xDICOM) = shift_xDICOM/cos(BeamAngle)
% shift_xBEV(due to yDICOM) = shift_yDICOM/sin(BeamAngle)
% shift_yBEV = shift_zDICOM (always, as long as CouchAngle = 0)

BeamAngleRad = GantryAngle * (pi/180); % degrees to radians

% Calcualte shift BEV (x and y) components for each scenario:
% NOTE: when the resulting shift is in the direction of the beam it is
% considered zero!
%           [       shift_xBEV                             shift_yBEV ]
    
    if ((GantryAngle  == 0) || (GantryAngle == 180))
        
        shiftBEV = [                                             
                  SystSetUpError(1)/cos(BeamAngleRad)         0
                  SystSetUpError(2)/cos(BeamAngleRad)         0
                                   0                               0
                                   0                               0
                                   0                      SystSetUpError(5)
                                   0                      SystSetUpError(6)];
                               
    elseif ((GantryAngle == 90) || (GantryAngle == 270))
        
        shiftBEV = [                                             
                                   0                               0
                                   0                               0
                  SystSetUpError(3)/sin(BeamAngleRad)         0
                  SystSetUpError(4)/sin(BeamAngleRad)         0
                                   0                      SystSetUpError(5)
                                   0                      SystSetUpError(6)];
    else
        
    
    shiftBEV = [                                             
                  SystSetUpError(1)/cos(BeamAngleRad)                0
                  SystSetUpError(2)/cos(BeamAngleRad)                0
                  SystSetUpError(3)/sin(BeamAngleRad)                0
                  SystSetUpError(4)/sin(BeamAngleRad)                0
                                   0                      SystSetUpError(5)
                                   0                      SystSetUpError(6)];
    end
    

    disp('Setup error shift scenarios in BEV (in mm):  [shift_xBEV     shift_yBEV] ');
    disp([' > scenario 1 (+ xDICOM: R --> L):   [',num2str(shiftBEV(1,1)),',',num2str(shiftBEV(1,2)),']']);
    disp([' > scenario 2 (- xDICOM: L --> R):   [',num2str(shiftBEV(2,1)),',',num2str(shiftBEV(2,2)),']']);
    disp([' > scenario 3 (+ yDICOM: A --> P):   [',num2str(shiftBEV(3,1)),',',num2str(shiftBEV(3,2)),']']);
    disp([' > scenario 4 (- yDICOM: P --> A):   [',num2str(shiftBEV(4,1)),',',num2str(shiftBEV(4,2)),']']);
    disp([' > scenario 5 (+ zDICOM: I --> S):   [',num2str(shiftBEV(5,1)),',',num2str(shiftBEV(5,2)),']']);
    disp([' > scenario 6 (- zDICOM: S --> I):   [',num2str(shiftBEV(6,1)),',',num2str(shiftBEV(6,2)),']']);
    
    interp_shiftBEV(:,1) = round(shiftBEV(:,1)/HexagonalGrid.voxelSize(2))*HexagonalGrid.voxelSize(2);
    interp_shiftBEV(:,2) = round(shiftBEV(:,2)/HexagonalGrid.voxelSize(1))*HexagonalGrid.voxelSize(1);

end

