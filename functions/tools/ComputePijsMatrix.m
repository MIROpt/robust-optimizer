%% ComputePijsMatrix
% This function collects the information needed to compute the spots doses
% (the so-called dose influence matrix: Pij) and calls MCsquare to perform
% the dose calculation.
%%

%% Syntax
% | [ warm_start_in,Plan ] = ComputePijsMatrix( Plan, handles )|

%% Description
% | [ warm_start_in,Plan ] = ComputePijsMatrix( Plan, handles )| 
% collects the information stored in REGGUI |handles| about the CT image, as
% well as the plan parameters contained in the MIROpt |Plan| structure. This
% information is passed to MCsquare to compute the dose influence matrix,
% which will be also stored in the |Plan| structure (|Plan.Scenario4D|).

%% Input arguments
% |Plan| - _struct_ - MIROpt structure where all the plan parameters are
% stored. The following data must be present in the structure:
%
% * |Plan.BeamletsBy| - _string_ - Name of the dose engine that wants to be used to compute the spots. So far, there is only one dose engine supported, and therefore, the parameter must be set equal to 'MCsquare'.
% * |Plan.BDL| - _string_ - Name of the beam model. See the list of supported beam models for the dose engine.
% * |Plan.CTinfo| - _struct_ - Structure containing all the information    stored in the CT DICOM header.
% * |Plan.output_path| - _string_ - Path indicating the name and location of the file where all the output data must be stored.
% * |Plan.Nbr4DScenarios| - _scalar_ - Number of scenarios used to simulate the breathing motion. 
% * |Plan.Plan.NbrRandomScenarios| - _scalar_ - Number of scenarios used to simulate random setup errors.
% * |Plan.Opt4Dmode| - _scalar_ - Boolean value indicating if the 4D dose mode must be used to compute the dose influence matrix (equal to 1) or not (equal to zero).
% * |Plan.protonsBeamlet| - _scalar_ - Number of protons per spot used to compute the dose influence matrix.
%
% |handles| - _struct_ - REGGUI data structure containing relevant
% information about the CT and RT DICOM plan. The following data must be present:
%
% * |handles.images.name{2}| - _string_ - Name of the CT image.

%% Output arguments
% |Plan| - _struct_ - MIROpt structure where all the plan parameters are
% stored. 
% |warm_start_in| - _struct_ - Structure containing the information for the
% warm start option.

%% NOTE
% Note that 4D dose calculation is not supported so far.
%% Contributors
% Author(s): Ana Barragan, Lucian Hotoiu


function [ warm_start_in,Plan ] = ComputePijsMatrix( Plan, handles )

disp(['Computing beamlets dose from scratch by ',Plan.BeamletsBy,' ...']);
disp(['BDL model --> ',Plan.BDL]);

warm_start_in(1).status = 0;

% Save pseduo-plan (all spots weights = 1) in DICOM format
createDICOMPlan(Plan,Plan.CTinfo,Plan.output_path)

i = 2 ; % cause the first one is empty

% Calculate dose contribution matrix (given in Gy)
for i_4D = 1:Plan.Nbr4DScenarios
    for rr = 1:Plan.NbrRandomScenarios
        folderName = 'Beamlets';
        if(Plan.Opt4Dmode == 1)
            folderName = [folderName,'_4D',num2str(i_4D)];
        end
        if (Plan.NbrRandomScenarios > 1)
            folderName = [folderName,'_random',num2str(rr)];
        end
        
        % load data in handles using REGGUI function
        handles.plans.name{2} = 'Plan.dcm';
        [handles.plans.data{i},handles.plans.info{i}] = load_DICOM_RT_Plan(fullfile(Plan.output_path,handles.plans.name{2}));
        
        tic;
        switch Plan.BeamletsBy
            case 'MCsquare'
                
                %Calculate_MC2_beamlets( PlanFile,Plan,rr,i_4D,folderName);
                handles = MC2_spots_simulation_MIROpt(handles,handles.images.name{2},'Plan.dcm',fullfile(Plan.output_path,folderName),Plan,rr,i_4D,Plan.protonsBeamlet,'sparse');
                % Delete tmp file that pops out when using
                % BeamletsParalellization = 1 in MCsquare
%                 if exist([folderName,'/Outputs/tmp'],'dir') == 7
%                     rmdir([folderName,'/Outputs/tmp'],'s')
%                 end
%             case 'FoCa'
%                 Calculate_FoCa_beamlets( PlanFile,Plan,rr,i_4D,folderName);
%             case 'AnalyticMCsquare'
%                 AnalyticMCsquare(PlanFile,Plan,OptConfig,folderName);
                
        end
        t=toc;
        disp(['Total beamlets computation time = ',num2str(t)]);
        
    end
end

Plan.PlanExistentFile = Plan.output_path;

end

