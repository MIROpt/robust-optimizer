%% DICOMtoBEV
% This function performs a rotation of the input matrix to transform the
% reference coordinate system from DICOM to BEV (Beam Eye View), which is
% perpendicular to the beam direction.
%%

%% Syntax
% |[ B ] = DICOMtoBEV( theta, imgCT_RTSTRUCT, Isocenter, A)|

%% Description
% |[ B ] = DICOMtoBEV( theta, imgCT_RTSTRUCT, Isocenter, A)| transforms the
% input matrix |A|, in DICOM coordinate system, to a matrix |B| in the BEV
% system.

%% Input arguments
% |theta| - _scalar_ - Gantry angle for current beam (in degrees).
%
% |imgCT_RTSTRUCT| - _struct_ - Structure containing the CT matrix and the related information. The following data must be present in the structure:
%
% * |imgCT_RTSTRUCT.dpcx| - _array_ - Column vector containing the x-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dpcy| - _array_ - Column vector containing the y-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dpcz| - _array_ - Column vector containing the z-DICOM coordinate for the center of each voxel of the CT (in mm).
% * |imgCT_RTSTRUCT.dspa| - _array_ - Column (3x1) vector containing the resolution of the CT for the x, y and z DICOM coordinates.
%
% |Isocenter| - _array_ - Column (3x1) vector containing
%   the coordinates for the isocenter of the current beam, in DICOM system.
%
% |A| - _array_ - Three-dimensional matrix containing relevant information
% in DICOM coordinate system that needs to be transformed in BEV system.


%% Output arguments
% |B| - _array_ - Three-dimensional matrix containing the input information
% transformed in BEV system.

%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu


function [ B ] = DICOMtoBEV( theta, imgCT_RTSTRUCT,Isocenter, A)

% to have DICOM layout 
A = permute(A,[2 1 3]);

DICOMcorner = [imgCT_RTSTRUCT.dpcy(1)-imgCT_RTSTRUCT.dspa(2),...
               imgCT_RTSTRUCT.dpcx(1)-imgCT_RTSTRUCT.dspa(1),...
               imgCT_RTSTRUCT.dpcz(1)-imgCT_RTSTRUCT.dspa(3)];


% Conversion to pixels units
DICOMcorner = [DICOMcorner(1)/imgCT_RTSTRUCT.dspa(2),...
               DICOMcorner(2)/imgCT_RTSTRUCT.dspa(1),...
               DICOMcorner(3)/imgCT_RTSTRUCT.dspa(3)];

Isocenter = [Isocenter(2)/imgCT_RTSTRUCT.dspa(2),...
             Isocenter(1)/imgCT_RTSTRUCT.dspa(1),...
             Isocenter(3)/imgCT_RTSTRUCT.dspa(3)];


% MATLAB does the rotation around a corner (0,0,0)  So:
% 1. Translation of isocenter to corner of rotation 
% 2. Rotate
% 3. Translation back to DICOM center
theta = -theta; % because z do not point towards the observer and it is a counter-clock wise rotation



% 1. 

T1A = [1 0 0         0
       0 1 0         0
       0 0 1         0
    - Isocenter      1];

T1B = [1 0 0         0
       0 1 0         0
       0 0 1         0
    + DICOMcorner 1];


% 2. Rotation of (dilated) target mask around Z(dicom) axis
% important: Z is inverted with respect to DICOM coord, so that we have a
% right handed system. It is inverted when using open_drt to read.
% INFO: rotation matrix around z axis
% Rotation appears counter-clockwise when the axis about which they occur
% (z in this case) points toward the observer, the coordinate system is
% right-handed, and the angle θ is positive.

T2 = [ cos(theta)  -sin(theta) 0 0
       sin(theta)   cos(theta) 0 0
            0             0    1 0
            0             0    0 1];
        
% 3.

T3A = [1 0 0         0
       0 1 0         0
       0 0 1         0
    - DICOMcorner 1];


T3B = [1 0 0         0
       0 1 0         0
       0 0 1         0
    + Isocenter      1];

% Total transformation matrix

%T = T1A * T1B * T2 * T3A; % BEV coord. :) , image center at isocenter


T = T1A * T1B * T2 * T3A * T3B; % image center at DICOM center

tform = maketform('affine', T);
R = makeresampler('nearest', 'replicate');


TDIMS_A = [1 2 3];
TDIMS_B = [1 2 3];
TSIZE_B = size(A);
TMAP_B = [];
F = 0;

B = tformarray(A, tform, R, TDIMS_A, TDIMS_B, TSIZE_B, TMAP_B, F);
    


end

