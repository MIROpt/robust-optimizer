function [ handles ] = ComputeFinalDose( Plan, handles)

i = 2 ; % cause the first one is empty

% % load data in handles using REGGUI function
handles.plans.name{2} = 'Plan.dcm';
[handles.plans.data{i},handles.plans.info{i}] = load_DICOM_RT_Plan(fullfile(Plan.output_path,handles.plans.name{2}));

% input arguments for MCsquare simulation
SimuParam.RunSimu = 1;
SimuParam.Folder = Plan.output_path;
SimuParam.CT = handles.images.name{2};%Plan.CT_path;
SimuParam.Plan = 'Plan.dcm';
SimuParam.Scanner = Plan.ScannerDirectory;
SimuParam.BDL = Plan.BDL;
SimuParam.NumParticles = Plan.protonsFullDose;
SimuParam.NumCPU = 0;
SimuParam.Dose = 1;
SimuParam.DoseName = 'FinalDose';
SimuParam.Energy = 0;
%SimuParam.EnergyName = 'MCsquare_energy';
SimuParam.LET = 0;
SimuParam.DoseToWater = 0;
SimuParam.CropBody = 0;
SimuParam.BodyContour = '';
SimuParam.OverwriteHU = {};

handles = MC2_simulation(handles,SimuParam);
end

