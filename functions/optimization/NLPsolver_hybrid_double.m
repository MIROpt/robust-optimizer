function [x, info] = NLPsolver_hybrid_double(ROI,Plan,Outputs,OptConfig,x0,s_nominal)

% Total number of scenarios
Plan.nscenarios = Plan.NbrRangeScenarios * Plan.NbrSystSetUpScenarios * Plan.NbrRandomScenarios * Plan.Nbr4DScenarios;

% global variables
backUp.scenarioDose = struct('D',sparse(zeros(Plan.DoseGrid.nvoxels,1)));
wBackUp_counter = 1; 
wBackUp_Niter = 25; % save w values and plot DVH for target volume every Niter iterations
if (OptConfig.plotTargetDVH == 1)
    h = figure;
    title('DVH for target volume');
    xlabel('Dose (Gy)');
    ylabel('% Volume');
    axis([0,100,0,100]);
end
globalIter = 0;
MRO_counter = 0;


%% ============ Mixed optimization settings ============= %

MixOpt_Niter =OptConfig.mixedOptimization.iter;
MixOpt_nCorrections = OptConfig.mixedOptimization.MC_corrections;

% Initialize correction matrix (C) for each scenario
for s = 1:Plan.nscenarios
    backUp.scenarioDose(s).MixOpt_C = zeros(Plan.DoseGrid.nvoxels,1);
end

% Options
options.ipopt.bound_frac = 0.01; %default 0.01
options.ipopt.bound_push = 0.001; %default 0.01
    
  
%% Set the IPOPT options.

% minMU = 0.02; % minimum weight value for Universal Nozzle 0.02 MU
minMU = 0;
options.lb  = [0 minMU*ones(1,Plan.nominalSpots)];         % Lower bounds on x

% maxMU = 12;     % minimum weight value for Universal Nozzle 0.02 MU
maxMU = Inf;
options.ub  = [Inf maxMU*ones(1, Plan.nominalSpots)];      % Upper bounds on x

% The bound on constraint functions.
options.cl  = zeros(1, Plan.nscenarios);           % Lower bounds on c
options.cu  = ones(1, Plan.nscenarios)*(Inf);               % Upper bounds on c


% Output
options.ipopt.print_level = 5; %maximum print level=12
options.ipopt.output_file = fullfile(Plan.output_path,'ipopt_output_file.txt');
options.ipopt.file_print_level = 5;
options.ipopt.print_info_string = 'yes';
options.ipopt.print_timing_statistics = 'yes';

% Termination
options.ipopt.tol              = 1e-8; % default 1e-8
options.ipopt.max_iter         = 3000; % default 3000
options.ipopt.dual_inf_tol     = 1; % default 1
options.ipopt.constr_viol_tol  = 0.0001; % default 0.0001
options.ipopt.compl_inf_tol    = 0.0001; % default 0.0001

% Hessian perturbation

% Quasi-Newton
options.ipopt.hessian_approximation = 'limited-memory'; % default 'exact'
options.ipopt.limited_memory_update_type = 'bfgs';
options.ipopt.limited_memory_max_history =1000;

% DERIVATIVE CHECK
%options.ipopt.derivative_test = 'first-order'; % Derivatives are checked so far and there are ok, values for relative diff. range from 1e-2 to 1e-4



%% Print IPOPT options

save('NLPsolver_options.mat','options', '-v7.3');


%% Callback functions.
funcs.objective         = @objective;
funcs.constraints       = @constraints;
funcs.gradient          = @gradient;
funcs.jacobian          = @jacobian;
funcs.jacobianstructure = @jacobianstructure;
funcs.iterfunc          = @getOptVars;

backUp.w = ones(size(x0(2:end)))*(-1); % so that the backUp dose is calculated before entering the loop

%% Run IPOPT.

% call MC forward dose calculation as many times as set in MixOpt_nCorrections

while MRO_counter < MixOpt_nCorrections
    MRO_counter = MRO_counter + 1;
    checkBackUp(x0);
    ComputeMCDose(x0); % it calculates backUp.scenarioDose(s).MixOpt_C for all s
    tic;
    [x, info] = ipopt(x0,funcs,options); 
    t=toc;
    disp(['Optimization time = ',num2str(t)]);
    x0 = x;
    wCorrected = x(2:end);
    save(['wCorrected',num2str(MRO_counter)],'wCorrected');
end



%% IPOPT functions: constraints, derivatives, dose calculation, etc..


% ----------------------------------------------------------------------
function checkBackUp(x)

w = x(2:end);

% If weights vector has changed, compute the new dose
if any(w ~= backUp.w)
    s = 1;
    for i_4D = 1:Plan.Nbr4DScenarios
        for rr = 1:Plan.NbrRandomScenarios
            for ss = 1:Plan.NbrSystSetUpScenarios % syst. setup scenarios = ss
                Plan.SystSetUpScenario(ss).ws(Plan.SystSetUpScenario(ss).wID) = w;
                for rs = 1:Plan.NbrRangeScenarios % range scenario = rs
                    % 1D vector for the total dose in CT
                    backUp.scenarioDose(s).D = backUp.scenarioDose(s).MixOpt_C + sparsedoublematrixvectorproduct_methodA_parallel(Plan.Scenario4D(i_4D).RandomScenario(rr).RangeScenario(rs).P, Plan.SystSetUpScenario(ss).ws);
                    s = s + 1;
                end
            end
        end
    end
    
    backUp.w = w;
end

end

% ------------------------------------------------------------------
function f = objective (x)
f = x(1);
end


% ------------------------------------------------------------------
function c = constraints (x)
checkBackUp(x)

c = ones(Plan.nscenarios,1)*x(1);
s = 1;
for i_4D = 1:Plan.Nbr4DScenarios
    for rr = 1:Plan.NbrRandomScenarios
        for ss = 1:Plan.NbrSystSetUpScenarios % syst. setup scenarios = ss
            for rs = 1:Plan.NbrRangeScenarios % range scenario = rs
                for i = 1:length(Plan.optFunction)
                    if (Plan.optFunction(i).robust == 1)
                        c(s) = c(s) - evalFunction(Plan.optFunction(i),backUp.scenarioDose(s).D);
                    else
                        if (s == s_nominal(i_4D))
                            c(s) = c(s) - evalFunction(Plan.optFunction(i),backUp.scenarioDose(s).D);
                        %else
                            %c(s) = c(s) - 0;
                        end
                    end
                end
                s = s + 1;
            end
        end
    end
end

end


% ------------------------------------------------------------------
function g = gradient (x)   
g = [1 zeros(1,length(x) - 1)];
end

% ------------------------------------------------------------------
function J = jacobianstructure ()
J = sparse(ones(Plan.nscenarios,Plan.nominalSpots + 1));
end

% ------------------------------------------------------------------
function J = jacobian (x)
checkBackUp(x);

J = zeros(Plan.nscenarios,Plan.nominalSpots + 1);

% One row for each scenario, one column for each variable.
% Two strategies implemented: A and B. B seems to be slightly faster than A.
s = 1;
for i_4D = 1:Plan.Nbr4DScenarios
    for rr = 1:Plan.NbrRandomScenarios
        for ss = 1:Plan.NbrSystSetUpScenarios % syst. setup scenarios = ss
            for rs = 1:Plan.NbrRangeScenarios % range scenario = rs
                
                J(s,1) = 1; % derivative with respect to the auxiliar variable t
                % initialize fder
                %fder = sparse(Plan.DoseGrid.nvoxels,1); %A
                fder = 0;%B
                for i = 1:length(Plan.optFunction)
                    if (Plan.optFunction(i).robust == 1)
                        %A
                        %fder = fder - sparse(ROI(Plan.optFunction(i).ROIindex).voxelsID,1,evalDerivative(Plan.optFunction(i),backUp.scenarioDose(s).D),Plan.DoseGrid.nvoxels,1);
                        %B
                        tmp = zeros(Plan.DoseGrid.nvoxels,1);
                        tmp(ROI(Plan.optFunction(i).ROIindex).mask1D) = evalDerivative(Plan.optFunction(i),backUp.scenarioDose(s).D);
                        fder = fder - tmp;
                    else
                        if (s == s_nominal(i_4D))
                            %A
                            %fder = fder - sparse(ROI(Plan.optFunction(i).ROIindex).voxelsID,1,evalDerivative(Plan.optFunction(i),backUp.scenarioDose(s_nominal(i_4D)).D),Plan.DoseGrid.nvoxels,1);
                            %B
                            tmp = zeros(Plan.DoseGrid.nvoxels,1);
                            tmp(ROI(Plan.optFunction(i).ROIindex).mask1D) = evalDerivative(Plan.optFunction(i),backUp.scenarioDose(s_nominal(i_4D)).D);
                            fder = fder - tmp;
                        %else
                            %fder = fder - 0;
                        end
                    end
                end
                %fder = full(fder);%A
                %
                J(s,2:end) = sparsedoublematrixvectorproduct_fderP_methodA_parallel(Plan.Scenario4D(i_4D).RandomScenario(rr).RangeScenario(rs).P(:,Plan.SystSetUpScenario(ss).wID),fder);
                s = s + 1;
            end
        end
    end
end

J =  sparse(J);
end


% -----------------------------------------------------------------
function next = getOptVars (t, f, varstruct)
% INPUT: 
% t = current iteration number
% f = current objective function value
% varstruct = struct containing the fields: x, inf_pr,inf_du,mu,d_norm,regularization_size,alpha_du,alpha_pr and ls_trials.

globalIter = globalIter + 1;

next = true;


if (t == wBackUp_counter*wBackUp_Niter)
    wBackUp_counter = wBackUp_counter + 1;
    %save w to file
    w = varstruct.x(2:end);
    save('w','w');
    
    % Plot target DVH in nominal case
    if (OptConfig.plotTargetDVH == 1)
        d = backUp.scenarioDose(s_nominal).D(logical(ROI(Plan.TargetROI_ID).mask1D));
        int_bins = min(d):0.005:max(d); %bin centers
        int_hist = hist(d,int_bins); % in the future we have to take into account the relative volume of each voxel (SEE plotQvhGeneral in DPBNmodule)
        DVH = 100/sum(int_hist)*flipdim(cumsum(flipdim((int_hist'),1)),1);
        h = plot(int_bins',DVH,  'g-');
        drawnow;
    end
    
end

if (globalIter == MRO_counter*MixOpt_Niter)
    % disp('*MixROpt* --> Computing MC forward dose');
    next = false;
end

end

% ----------------------------------------------------------------------
function fvalue = evalFunction(optFunction,D)

% This function evaluates the penalty function associated to each ROI 

ROIindex = optFunction.ROIindex;
d = D(ROI(ROIindex).mask1D);

  
if  (optFunction.ID == 1) % 'min'

    tmp = max(0, optFunction.Dref - d);
    fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*sum(tmp.*tmp); % faster

    %diff = optFunction.Dref - d;
    %diff = diff(logical(diff>0));
    %fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*sum(diff.*diff);

elseif (optFunction.ID == 2) % 'max'

    tmp = max(0, d - optFunction.Dref);
    fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*sum(tmp.*tmp); % faster

    %diff = d - optFunction.Dref;
    %diff = diff(logical(diff>0));
    %fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*sum(diff.*diff);

elseif (optFunction.ID == 3) % 'min_mean'

    fvalue = optFunction.impw*max(0,optFunction.Dref - mean(d));

elseif (optFunction.ID == 4) % 'max_mean'

    fvalue = optFunction.impw*max(0,mean(d) - optFunction.Dref);

elseif (optFunction.ID == 5) % 'minDVH'   

    diff = optFunction.Dref - d;
    dvh_values = sort(d,'descend');
    % compute current dose at Vref
    Dc = dvh_values(max([1 round(optFunction.Vref*ROI(ROIindex).nvoxels)])); % 
    diff = diff(d < optFunction.Dref | d > Dc);
    %diff(d > optFunction.Dref | d < Dc) = 0;
    fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*(diff'*diff);

elseif (optFunction.ID == 6) % 'maxDVH'  
    
     diff = d - optFunction.Dref;
     dvh_values = sort(d,'descend');
     % compute current dose at Vref
     Dc = dvh_values(max([1 round(optFunction.Vref*ROI(ROIindex).nvoxels)])); % 
     diff = diff(d > optFunction.Dref | d < Dc);
     %diff(d < optFunction.Dref | d > Dc) = 0;
     fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*(diff'*diff); % PROBLEM
end
    
end
% ---------------------------------------------------------------------

function fder = evalDerivative(optFunction,D)
ROIindex = optFunction.ROIindex;
d =D(ROI(ROIindex).mask1D);


if (optFunction.ID == 1) % 'min'

    diff = optFunction.Dref - d;
    diff (diff < 0) = 0;
    fder = (optFunction.impw/ROI(ROIindex).nvoxels)*(-2 * diff);

elseif (optFunction.ID == 2) % 'max'

    diff = d - optFunction.Dref;
    diff(diff < 0) = 0;
    fder = (optFunction.impw/ROI(ROIindex).nvoxels)*(2 * diff);

elseif (optFunction.ID == 3) % 'min_mean'

    fder = (optFunction.impw/ROI(ROIindex).nvoxels)*ones(ROI(ROIindex).nvoxels,1);

elseif (optFunction.ID == 4) % 'max_mean'

    fder = (optFunction.impw/ROI(ROIindex).nvoxels)*ones(ROI(ROIindex).nvoxels,1);

elseif (optFunction.ID == 5) % 'minDVH'

     diff = optFunction.Dref - d;
     dvh_values = sort(d,'descend');
     % compute current dose at Vref
     Dc = dvh_values(max([1 round(optFunction.Vref*ROI(ROIindex).nvoxels)])); 
     diff(d > optFunction.Dref | d < Dc) = 0; % need to have the same size as mask1D
     fder = (optFunction.impw/ROI(ROIindex).nvoxels)*(-2* diff);

elseif (optFunction.ID == 6) % 'maxDVH'

     diff = d - optFunction.Dref;
     dvh_values = sort(d,'descend');
     % compute current dose at Vref
     Dc = dvh_values(max([1 round(optFunction.Vref*ROI(ROIindex).nvoxels)])); 
     diff(d > optFunction.Dref | d < Dc) = 0; % need to have the same size as mask1D
     fder = (optFunction.impw/ROI(ROIindex).nvoxels)*(2* diff); %PROBLEM

end
end

% ---------------------------------------------------------------------
function ComputeMCDose(x)
    

disp('Calculate MC dose for:');
disp(['Random Error (mm) = ', num2str(OptConfig.mixedOptimization.RandSetUpError)]);
if (OptConfig.mixedOptimization.Opt4D.mode == 1)
    disp('4D mode = ON');
    disp(['Data from ', OptConfig.mixedOptimization.Opt4D(1).DirDeformFields]);
else
    disp('4D mode OFF');
end
disp(['Range Error (%) = ',num2str(Plan.RangeError)]);
disp(['Syst. Error (mm) = ',num2str(Plan.SystSetUpError)]);


w = x(2:end);

% Save current values for spot weights into PlanFile

idx_start = 1;
idx_end = 0;
for b = 1: length(Plan.Beams)
    for l = 1:length(Plan.Beams(b).Layers)
        idx_end = idx_end + size(Plan.Beams(b).Layers(l).nominalSpotPosition,1);
        Plan.Beams(b).Layers(l).SpotWeights = w(idx_start:idx_end);
        idx_start = idx_end + 1;
    end
end

s = 1;
for i_4D = 1:Plan.Nbr4DScenarios
    for rr = 1:Plan.NbrRandomScenarios
        for ss = 1:Plan.NbrSystSetUpScenarios % syst. setup scenarios = ss
            
            if (ss == 1) % nominal scenario
                for b = 1: length(Plan.Beams)
                    for l = 1:length(Plan.Beams(b).Layers)
                        Plan.Beams(b).Layers(l).SpotPositions = [];
                        Plan.Beams(b).Layers(l).SpotPositions(:,1) = Plan.Beams(b).Layers(l).nominalSpotPosition(:,1);
                        Plan.Beams(b).Layers(l).SpotPositions(:,2) = Plan.Beams(b).Layers(l).nominalSpotPosition(:,2);
                    end
                end
            else
                
                for b = 1: length(Plan.Beams)
                    for l = 1:length(Plan.Beams(b).Layers)
                        Plan.Beams(b).Layers(l).SpotPositions = [];
                        % I use the interpolated shift instead of the real one to avoid problems in the dose when using the mixed optimization
                        Plan.Beams(b).Layers(l).SpotPositions(:,1) = Plan.Beams(b).Layers(l).nominalSpotPosition(:,1) - Plan.Beams(b).interp_shiftBEV(Plan.SystSetUpShiftID(ss-1),1);
                        Plan.Beams(b).Layers(l).SpotPositions(:,2) = Plan.Beams(b).Layers(l).nominalSpotPosition(:,2) - Plan.Beams(b).interp_shiftBEV(Plan.SystSetUpShiftID(ss-1),2);
                    end
                end
            end
            
            
            [Plan_info, PlanFile] = SavePBSPlan(Plan,Plan.CTinfo,'MCsquare'); % need to provide Plan.CTinfo (save into auxvars maybe since it s gonna be always the same)
            
            
            % Calculate forward dose with MCsquare for each ss scenario
            
            NbrProtons = 1e7;
            Dose = CalculateMC2ForwardDose( Plan_info, PlanFile, OptConfig, Plan, NbrProtons);
            Plan.SystSetUpScenario(ss).ws(Plan.SystSetUpScenario(ss).wID) = w;
            for rs = 1: Plan.NbrRangeScenarios
                
                % IMPORTANT, MCsquare gives always the dose in Gy
                if isempty(Outputs.DVH_roiSet)
                    roiSet = (1:1:length(ROI)-1);
                else
                    roiSet = Outputs.DVH_roiSet; % to plot the DVH in the volumes of interest
                end
                Ds = sparsedoublematrixvectorproduct_methodA_parallel(Plan.Scenario4D(i_4D).RandomScenario(rr).RangeScenario(rs).P, Plan.SystSetUpScenario(ss).ws);
                
                % save MC dose
                save(['DMC_wCorrected_',num2str(MRO_counter),'_',num2str(s)] ,'Dose');
                save(['DPB_wCorrected_',num2str(MRO_counter),'_',num2str(s)],'Ds');
                plotDVH(Dose{rs},Ds,roiSet);
                
%               pause;
                % Compute new correction matrix
                % modification 31/08/2016
                %normF = mean(Dose{rs}(ROI(Plan.TargetROI_ID).mask1D))/mean(backUp.scenarioDose(s).D(ROI(Plan.TargetROI_ID).mask1D));
%                  dose_to_target = 74;
% %                 %D50 = prctile(backUp.scenarioDose(s).D,50); 
%                  normF = mean(Dose{rs}(ROI(Plan.TargetROI_ID).mask1D))/dose_to_target;
%                  w = w/normF;
%                  Dose = CalculateMC2ForwardDose( Plan_info, PlanFile, OptConfig, Plan, NbrProtons);
%                 %normF = mean(Dose{rs}(ROI(Plan.TargetROI_ID).mask1D))/mean(Ds(ROI(Plan.TargetROI_ID).mask1D));
%                 %normF = prctile(Dose{rs},50)/prctile(backUp.scenarioDose(s).D,50);
%                 Dose{rs} = Dose{rs}/normF;
%                 plotDVH(Dose{rs},Ds,roiSet);
%                 pause;
               
                
                % end modification
                backUp.scenarioDose(s).MixOpt_C = full(Dose{rs}) - Ds; % use P*wCorrected as reference
                % Reduce matrix
                if (s==s_nominal)
                    backUp.scenarioDose(s).MixOpt_C = backUp.scenarioDose(s).MixOpt_C.*Plan.OptROIVoxels_nominal;
                else
                    backUp.scenarioDose(s).MixOpt_C = backUp.scenarioDose(s).MixOpt_C.*Plan.OptROIVoxels_robust;
                end

                backUp.scenarioDose(s).D = Ds; % + backUp.scenarioDose(s).MixOpt_C?
                
                s = s + 1;
            end
        end
    end
end

end

% ----------------------------------------------------------------------
 function plotDVH(dForw, dPreForw, roiSet)

hm=figure;
hold on;
ROInames = cell(length(roiSet),1);
cmap = hsv(length(ROInames));  %# Creates set of colors from the HSV colormap

ROInames = cell(length(roiSet)*2,1);
for i = 1:length(ROInames)
    if mod(i,2) == 0
        ROInames{i} = ROI(roiSet(i/2)+1).name;
    else
    ROInames{i} = '';
    end
end


for roi = 1:length(roiSet)

    ROIidx = roiSet(roi);
    dose = cell(2,1);
    dose{1} = dForw(logical(ROI(ROIidx+1).mask1D));% Gy;
    lineType{1} = '-';
    dose{2} = dPreForw(logical(ROI(ROIidx+1).mask1D));% Gy;
    lineType{2} = '-.';

    for d = 1:length(dose)
       
        delta_bin=0.005;
        minDose = min(dose{d});
        maxDose = max(dose{d});

        int_bins = minDose:delta_bin:maxDose; %bin centers

        int_hist = hist(dose{d},int_bins);

        DVH = 100/sum(int_hist)*flipdim(cumsum(flipdim((int_hist'),1)),1);

        if ~isempty(DVH)

            plot(int_bins',DVH, lineType{d},'Color',cmap(roi,:));

        else
            plot([0 0],[0 100], lineType{d},'Color',cmap(roi,:));
        end
    end
end
title(['dvh - update Nº ',num2str(MRO_counter),'- scenario ',num2str(s)]);
xlabel('Dose (Gy)');
ylabel('% Volume');
%axis([0,maxDose,0,100]);
axis([0,Inf,0,100]);
legend(ROInames);
hold off;
pause(0.5);
saveas(hm,['dvh-',num2str(MRO_counter),'_',num2str(s),'.fig']);
end

  

end

    


