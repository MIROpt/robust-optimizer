function [ Plan ] = SpotWeightsOptimization(ROI,Plan,OptConfig,warm_start_in )

% call IPOPT to get optimal weights solution
if (OptConfig.BeamletsMatrixPrecision == 'f')
    [x, info] = NLPsolver_float(ROI,Plan,OptConfig,warm_start_in,Plan.s_nominal);
elseif (OptConfig.BeamletsMatrixPrecision == 'd')
    [x, info] = NLPsolver_double_matlabNative(ROI,Plan,OptConfig,warm_start_in,Plan.s_nominal);
end

% save optimal w values in Plan struct
w = x(2:end); % this is total weights (it should be divided between the number of fractions)
%wf = w/Plan.fractions;

idx_start = 1; 
idx_end = 0;

for b = 1: size(Plan.Beams,2)
    for l = 1:size(Plan.Beams(b).Layers,2)
        Plan.Beams(b).Layers(l).SpotPositions = Plan.Beams(b).Layers(l).nominalSpotPosition;
        idx_end = idx_end + size(Plan.Beams(b).Layers(l).SpotPositions,1);
        Plan.Beams(b).Layers(l).SpotWeights = w(idx_start:idx_end); 
        idx_start = idx_end + 1;
    end
end

% Save final plan in DICOM format
createDICOMPlan(Plan,Plan.CTinfo,Plan.PlanExistentFile)

end

