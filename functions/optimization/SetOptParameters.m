%% SetOptParameters
% This function process the information needed for the optimization.
%%

%% Syntax
% [Plan,ROI,warm_start_in] = SetOptParameters(OptConfig,Plan,ROI)

%% Description
% [Plan,ROI,warm_start_in] = SetOptParameters(OptConfig,Plan,ROI) computes
% some parameters needed for optimization, such as the index for the
% nominal case (|Plan.s_nominal|), the voxels involved in the optimization
% for the nominal case (|Plan.OptROIVoxels_nominal|) and all scenarios used
% in robust optimization (|Plan.OptROIVoxels_robust|), among other
% parameters (See Output arguments)

%% Input arguments
% |OptConfig| - _struct_ - Structure containing information needed to run 
% the optimization. The following data must be present in the structure:
%
% * |OptConfig.max_iter| - _scalar_ - Maximum number of iterations. If this number is reached, the optimization will stop.
% * |OptConfig.plotTargetDVH| - _scalar_ - Boolean value indicating if the DVH for the target must be plotted during the optimization (if equal to 1) or not (if equal to zero).
% * |OptConfig.BeamletMatrixPrecision| - _string_ - Parameter indicating the precision that must be used for the dose influence matrix. It has two values: 'd' if double precision and 'f' if float precision. The second one is recommended to reduce memory usage.
%
% * |OptConfig.mixedOptimization| - _struct_ - Structure containing information related to the mixed optimization. The following data must be present in the structure:
% * |mixedOptimization.ON| - _scalar_ - Boolean value indicating if the mixed optimization is on (if equal to 1) or off (if equal to zero).
% * |mixedOptimization.MC_corrections| - _scalar_ - Maximum number of corrections to be applied in the mixed optimization.
% * |mixedOptimization.iter| - _scalar_ - Maximum number of iterations to perform in between corrections, i.e., in the inner loop of the mixed optimization.
% * |mixedOptimization.RandSetUpError| - _array_ - Random setup errors are sampled for a Gaussian probability distribution. Therefore, this is a row (1x3) vector containing the standard deviation in x, y, and z DICOM axis to be applied in order to correct for random errors in the mixed optimization.
% * |mixedOptimization.Opt4D| - _struct_ - Structure containing information regarding the simulation of the breathing motion in order to correct for it using the mixed optimization. This feature is not supported so far.
%
% |Plan| - _struct_ - MIROpt structure where all the plan parameters are
% stored. The following data must be present in the structure:
%
% * |Plan.DoseGrid| - _struct_ - Structure containing the information about the dose grid. The following data must be present:
% * |Plan.DoseGrid.nvoxels| - _scalar_ - Total number of voxels of the dose grid, i.e., number of voxels in the CT image.
%
% * |Plan.optFunction| - _struct_ - Structure containing the information about the objective functions set by the user on the dose to the target volume and organs at risk. The following data must be present for each objective function with index i:
% * |Plan.optFunction(i).ROIname| - _string_ - Name of the ROI to which the objective function must be applied. 
% * |Plan.optFunction(i).name| - _string_ - Name of the objective function type. The possible types supported so far are: 'min', 'max', 'min_mean' and 'max_mean'.
% * |Plan.optFunction(i).robust| - _scalar_ - Boolean value indicating if the objective function must be considered in all scenarios (if set as robust, equal to 1), or only in the nominal case (if set as non-robust, equal to zero).
%
% * |Plan.NbrRandomScenarios| - _scalar_ - Number of scenarios used to simulate the random setup errors.
% * |Plan.NbrRangeScenarios| - _scalar_ - Number of scenarios used to simulate the range errors.
% * |Plan.Nbr4DScenarios| - _scalar_ - Number of scenarios used to simulate the errors related to breathing motion.
% * |Plan.NbrSystSetUpScenarios| - _scalar_ - Number of scenarios used to simulate the systematic setup errors.
% * |Plan.PlanExistentFile| - _string_ - Path pointing to the location of the file containing the spot doses. If empty, the spot doses (dose influence matrix) will be computed from scratch.
%
% |ROI| - _struct_ - MIROpt structure containing information about all
% volumes in the RTSTRUCT file. The following data must be present in the
% structure:
%
% * |ROI(i).name| - _string_ - Name of the i-th ROI in the RTSTRUCT list. This must be encoded for all structures, i. e., i = 1, ..., N; where N is the total number of ROIs.
% * |ROI(i).mask1D| - _array_ - Logical column vector storing a binary mask for ROI i (voxels inside the volume of interest are equal to 1, and those outside are equal to zero).
% * |ROI(i).nvoxels| - _scalar_ - Total number of voxels in the ROI i. At this stage this field is empty.
% * |ROI(i).voxelsID| - _array_ - Column vector containing the linear indices of the non-zero voxels in |ROI(i).mask1D|. At this stage this filed is empty.
%
%% Output arguments
% |Plan| - _struct_ - MIROpt structure where all the plan parameters are
% stored. The fields completed in this function are the following:
%
% * |Plan.OptROIVoxels_nominal| - _array_ - Logical column vector storing the voxels used for the optimization in the nominal case. This vector is used to read only the voxels which are really used, disregarding the rest, which helps to reduce memory usage with respect to the case where the dose in all the voxels of the CT image is read.
% * |Plan.OptROIVoxels_robust| - _array_ - Logical column vector storing the voxels used for the optimization in the robust case. This vector is used to read only the voxels which are really used, disregarding the rest, which helps to reduce memory usage with respect to the case where the dose in all the voxels of the CT image is read.
% * |Plan.optFunction(i).ROIindex| - _scalar_ - Index for the ROI to which the objective function is applied.
% * |Plan.optFunction(i).ID| - _scalar_ - Real number associated to the type of objective function selected. For instance, 'min' is equal to 1, 'max' is equal to '2'. This helps us to evaluate the function type faster since comparing strings is slower than comparing numbers.
% * |Plan.ss_nominal| - _scalar_ - Index for the nominal case with respect to systematic setup errors.
% * |Plan.rr_nominal| - _scalar_ - Index for the nominal case with respect to random setup errors.
% * |Plan.rs_nominal| - _scalar_ - Index for the nominal case with respect to range errors.
% * |Plan.s_nominal| - _scalar_ - Index for the global nominal case.
%
% |ROI| - _struct_ - MIROpt structure containing information about all
% volumes in the RTSTRUCT file. The following data must be present in the
% structure:
%
% * |ROI(i).name| - _string_ - Name of the i-th ROI in the RTSTRUCT list. This must be encoded for all structures, i. e., i = 1, ..., N; where N is the total number of ROIs.
% * |ROI(i).mask1D| - _array_ - Logical column vector storing a binary mask for ROI i (voxels inside the volume of interest are equal to 1, and those outside are equal to zero).
% * |ROI(i).nvoxels| - _scalar_ - Total number of voxels in the ROI i. 
% * |ROI(i).voxelsID| - _array_ - Column vector containing the linear indices of the non-zero voxels in |ROI(i).mask1D|. 
%
% |warm_start_in| - _struct_ - Structure containing the information for the
% warm start option.
%
%% NOTE
% Since only one dose calculation engine is implemented in this version of
% of the code (MCsquare), the hybrid or mixed optimization using two dose 
% engines is not supported. However, you can still perform hybrid
% optimization to include new sources of uncertainties, such as random
% errors or breathing motion [1]
% [1] WE-AB-209-01: A Monte Carlo-Based Method to Include Random Errors in
% Robust Optimization, A Barragan Montero, K Souris, J Lee, E Sterpin
% DOI: 10.1118/1.4957770
%
%% Contributors
% Authors : Ana Barragan, Lucian Hotoiu

% ---------------------- %
function [Plan,ROI,warm_start_in] = SetOptParameters(OptConfig,Plan,ROI)

% Get voxels involved in the optimization
Plan.OptROIVoxels_nominal = false(Plan.DoseGrid.nvoxels,1); % initialize to zeros
Plan.OptROIVoxels_robust = false(Plan.DoseGrid.nvoxels,1); % initialize to zeros

% loop over all objective functions defined by the user
for i = 1:length(Plan.optFunction)
    
    % find ROIindex
    id = getROIByName(ROI, Plan.optFunction(i).ROIname);
    Plan.optFunction(i).ROIindex = id;
    %Get OptROIVoxels_nominal and robust
    Plan.OptROIVoxels_nominal = Plan.OptROIVoxels_nominal | ROI(id).mask1D; % for all ROIs
    if (Plan.optFunction(i).robust == 1)
        Plan.OptROIVoxels_robust = Plan.OptROIVoxels_robust | ROI(id).mask1D; % only ROIs selected as robust
    end
    
    % If data for this ROI has not been already calculated, 
    if isempty(ROI(id).nvoxels)
        % calculate number of total voxels
        ROI(id).nvoxels = sum(ROI(id).mask1D);
    end
    if isempty(ROI(id).voxelsID)
        % calculate number of total voxels
        ROI(id).voxelsID = find(ROI(id).mask1D);
    end
    
    % set an index for the type of objective function (this will speed up
    % slightly the optimization)

    if isequal(Plan.optFunction(i).name, 'min')
        Plan.optFunction(i).ID = 1;
    elseif isequal(Plan.optFunction(i).name, 'max')
        Plan.optFunction(i).ID = 2;
    elseif isequal(Plan.optFunction(i).name, 'min_mean')
        Plan.optFunction(i).ID = 3;
    elseif isequal(Plan.optFunction(i).name, 'max_mean')
        Plan.optFunction(i).ID = 4;
    elseif isequal(Plan.optFunction(i).name, 'minDVH')
        Plan.optFunction(i).ID = 5;
    elseif isequal(Plan.optFunction(i).name, 'maxDVH')
        Plan.optFunction(i).ID = 6;
    else
        error('Not supported objective function')
    end
    


end

% Calculate index for nominal case
Plan.ss_nominal = 1; % nominal index for systematic setup (first case = nominal case, SEE ligne 683)
% get rr_nominal
Plan.rr_nominal = 0;
if (Plan.NbrRandomScenarios > 1)
    for rr = 1:Plan.NbrRandomScenarios
        if (mean(Plan.RandSetUpError{rr}) == 0) % It is supposed that when having diff. random errors, one of them will be the nominal case (no error, sigma = 0)
            Plan.rr_nominal = rr;
        end
    end
    if (Plan.rr_nominal == 0)
        error(' No nominal case for random error has been introduced ');
    end
else
    Plan.rr_nominal = 1; %
end

% get rs_nominal
if Plan.NbrRangeScenarios == 1
    Plan.rs_nominal = 1;    
elseif Plan.NbrRangeScenarios == 3
    Plan.rs_nominal = 2; % because MCsquare gives the scenarios ordered as: -3% 0 +3%
end

% get s_nominal

% There will be a s_nominal for each 4D scenario, which corresponds to
% shift=0, range=0 and sigma(random)=0 in case there are several sigmas
% or either the given sigma(random) if there is only one.

Plan.s_nominal = zeros(length(Plan.Nbr4DScenarios),1);
s = 1;
for i_4D = 1:Plan.Nbr4DScenarios % 4D scenarios
    for rr = 1:Plan.NbrRandomScenarios % random scenarios
        for ss = 1:Plan.NbrSystSetUpScenarios % syst. setup scenarios = ss
            for rs = 1:Plan.NbrRangeScenarios % range scenario = rs
                if (rs == Plan.rs_nominal && rr == Plan.rr_nominal && ss == Plan.ss_nominal) % For the moment we have a 'nominal' matrix for each 4D scenario when rs and rr are nominal
                    Plan.s_nominal(i_4D) = s;
                end
                s = s + 1;
            end
        end
    end
end

% Optimization settings

disp(['Maximum iter set to ',num2str(OptConfig.max_iter)]);

% Mixed robust optimization

if (OptConfig.mixedOptimization.ON == 1)
    
    disp('Mixed optimization is ON with: ');
    disp([' > MC corrections = ',num2str(OptConfig.mixedOptimization.MC_corrections)]);
    disp([' > Iterations before next MC run = ',num2str(OptConfig.mixedOptimization.iter)]);

elseif (OptConfig.mixedOptimization.ON == 0)
    disp('Mixed optimization is OFF ');
end

% warm_start
warm_start_in = struct('status',[], 'lambda',[], 'zl',[],'zu',[], 'varstruct',{});

% check if there is an existent BeamletsFile to use
if isfield(Plan,'PlanExistentFile')
    if isempty(Plan.PlanExistentFile)
        disp('No path for PlanExistentFile has been introduced')
    else
        if exist('warm_start_out.mat', 'file') == 2
            prompt =  'Warm start info found. Would you like to continue with previous optimization? Y/N :';
            warmstart = input(prompt, 's');
            if isempty(warmstart)
                warmstart = 'N';
            end
            if warmstart == 'Y'
                load('warm_start_out.mat');
                warm_start_in = warm_start_out;
                warm_start_in(1).status = 1;
                disp('Using warm start info to continue previous optimization...');
                
            elseif warmstart == 'N'
                warm_start_in(1).status = 0;
            else
                disp ('No valid input, using default setting (N)');
                warm_start_in(1).status = 0;
            end
        else
            disp('Warm start info was not found.')
            warm_start_in(1).status = 0;
        end
    end
end
end

