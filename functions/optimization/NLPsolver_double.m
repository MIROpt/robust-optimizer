function [x, info] = NLPsolver_double(ROI,Plan,OptConfig, warm_start_in,s_nominal)

% Total number of scenarios
Plan.nscenarios = Plan.NbrRangeScenarios * Plan.NbrSystSetUpScenarios * Plan.NbrRandomScenarios * Plan.Nbr4DScenarios;

% global variables
store_fobj = 0;
backUp.scenarioDose = struct('D',sparse(zeros(Plan.DoseGrid.nvoxels,1)));
backUp.w = ones(1,Plan.nominalSpots)*(-1);% to force to calculate the dose at the first iteration
wBackUp_counter = 1; 
wBackUp_Niter = 25; % save w values and plot DVH for target volume every Niter iterations
if (OptConfig.plotTargetDVH == 1)
    h = figure;
    title('DVH for target volume');
    xlabel('Dose (Gy)');
    ylabel('% Volume');
    axis([0,100,0,100]);
end

if warm_start_in(1).status == 1

    disp(' Using warm start info to continue previous optimization...');

    % Initialize the dual point.
    options.zl     = warm_start_in.zl';
    options.zu     = warm_start_in.zu';
    options.lambda = warm_start_in.lambda;
    % Initialize the primal variables
    x0 = warm_start_in.varstruct.x;
      
    
    % Set IPOPT options
    
    % WARM START
    options.ipopt.warm_start_init_point = 'yes';
    options.ipopt.warm_start_bound_push = 1e-6; % default = 0.001
    options.ipopt.warm_start_mult_bound_push = 1e-6; % default = 0.001
    
    % Barrier parameter
    options.ipopt.mu_strategy = 'monotone';% default = 'monotone' 
    options.ipopt.mu_init = warm_start_in.varstruct.mu; % default 0.1, recommended for warm start: 1e-6
    
else
   
    disp(' Starting optimization from scratch...');

    % The starting point. (ROW vector)
    w0 = ones(1,Plan.nominalSpots)*0.1;
    %w0 = initializeWeights(Plan.Beams,Plan.TVDosePrescription);
    
    % INITIALIZE t = objective function for the worst case scenario  
    checkBackUp([0 w0])
    fobj = zeros(Plan.nscenarios,1);  
    s = 1;
    for i_4D = 1:Plan.Nbr4DScenarios
        for rr = 1:Plan.NbrRandomScenarios
            for ss = 1:Plan.NbrSystSetUpScenarios % syst. setup scenarios = ss
                for rs = 1:Plan.NbrRangeScenarios % range scenario = rs

                    for i = 1:length(Plan.optFunction)
                        if (Plan.optFunction(i).robust == 1)
                            fobj(s) = fobj(s) + evalFunction(Plan.optFunction(i),backUp.scenarioDose(s).D);
                        else
                            if (s == s_nominal(i_4D))
                                fobj(s) = fobj(s) + evalFunction(Plan.optFunction(i),backUp.scenarioDose(s).D);
                            else
                                fobj(s) = fobj(s) + 0;
                            end
                        end
                    end
                    s = s + 1;
                end
                
            end
        end
    end
       
       
    % take worst case value
    t0 = max(fobj);

    %initialize variables
    x0 = [t0 w0];                               
    
    % Options
    
    % Initialization
    options.ipopt.bound_frac = 0.01; %default 0.01
    options.ipopt.bound_push = 0.001; %default 0.01
    
end
  
%% Set the IPOPT options.

% INFO (from G.Janssens)
% En ce qui concerne le système IBA, il y avait une limite de 12 MU/spot,
% mais depuis la dernière version du scanning controller software cette
% limite a disparu. Si la dose par spot est plus grande que 12 MU, le spot
% est répété. Parfois, les cliniciens peuvent eux-mêmes limiter la dose par
% spot dans le TPS. La limite inférieure est de 0.01 MU/spot pour la
% dedicated nozzle, et 0.02 MU/spot pour l’universal nozzle.


% minMU = 0.02; % minimum weight value for Universal Nozzle 0.02 MU
minMU = 0;
options.lb  = [0 minMU*ones(1,Plan.nominalSpots)];         % Lower bounds on x

% maxMU = 12; % minimum weight value for Universal Nozzle 0.02 MU
maxMU = Inf;
options.ub  = [Inf maxMU*ones(1, Plan.nominalSpots)];      % Upper bounds on x

% The bound on constraint functions.
options.cl  = zeros(1, Plan.nscenarios);           % Lower bounds on c
options.cu  = ones(1, Plan.nscenarios)*(Inf);               % Upper bounds on c


% Output
options.ipopt.print_level = 5; %maximum print level=12
options.ipopt.output_file = fullfile(Plan.output_path,'ipopt_output_file.txt');
options.ipopt.file_print_level = 5;
options.ipopt.print_info_string = 'yes';
options.ipopt.print_timing_statistics = 'yes';

% Termination
options.ipopt.tol              = 1e-8; % default 1e-8
options.ipopt.max_iter         = OptConfig.max_iter; % default 3000
options.ipopt.dual_inf_tol     = 1; % default 1
options.ipopt.constr_viol_tol  = 0.0001; % default 0.0001
options.ipopt.compl_inf_tol    = 0.0001; % default 0.0001


% Quasi-Newton
options.ipopt.hessian_approximation = 'limited-memory'; % default 'exact'
options.ipopt.limited_memory_update_type = 'bfgs';
% By using a big number we are forcing to use complete-memory bfgs since we
% take into accout all the previous iterations to compute the hessian
% update (BLGorissen advice)
options.ipopt.limited_memory_max_history =1000; 
%options.ipopt.limited_memory_max_history =20; 

% DERIVATIVE CHECK
%options.ipopt.derivative_test = 'first-order'; % Derivatives are checked so far and there are ok, values for relative diff. range from 1e-2 to 1e-4



%% Print IPOPT options

save('NLPsolver_options.mat','options', '-v7.3');


%% Callback functions.

funcs.objective         = @objective;
funcs.constraints       = @constraints;
funcs.gradient          = @gradient;
funcs.jacobian          = @jacobian;
funcs.jacobianstructure = @jacobianstructure;
funcs.iterfunc          = @getOptVars;


%% Define warm_start
warm_start_out = struct('status',[], 'lambda',[], 'zl',[],'zu',[], 'varstruct',{});

%% Run IPOPT.
tic;
[x, info] = ipopt(x0,funcs,options); 
t=toc;

disp(['Optimization time = ',num2str(t)]);

% save warm_start_out
warm_start_out(1).zl = info.zl;
warm_start_out(1).zu = info.zu;
warm_start_out(1).lambda = info.lambda;

save('warm_start_out','warm_start_out');

   
%% IPOPT functions: constraints, derivatives, dose calculation, etc..

% -----------------------------------------------------------------------
function checkBackUp(x)

w = x(2:end);

% If weights vector has changed, compute the new dose
if any(w ~= backUp.w)
    s = 1;
    for i_4D = 1:Plan.Nbr4DScenarios
        for rr = 1:Plan.NbrRandomScenarios
            for ss = 1:Plan.NbrSystSetUpScenarios % syst. setup scenarios = ss
                Plan.SystSetUpScenario(ss).ws(Plan.SystSetUpScenario(ss).wID) = w;
                for rs = 1:Plan.NbrRangeScenarios % range scenario = rs
                    % 1D vector for the total dose in CT
                    backUp.scenarioDose(s).D = sparsedoublematrixvectorproduct_methodA_parallel(Plan.Scenario4D(i_4D).RandomScenario(rr).RangeScenario(rs).P, Plan.SystSetUpScenario(ss).ws);
                    s = s + 1;
                end
            end
        end
    end
    backUp.w = w;
end

end

% ------------------------------------------------------------------
function f = objective (x)
f = x(1);
end


% ------------------------------------------------------------------
function c = constraints (x)
checkBackUp(x)
    
c = ones(Plan.nscenarios,1)*x(1);
s = 1;
for i_4D = 1:Plan.Nbr4DScenarios
    for rr = 1:Plan.NbrRandomScenarios
        for ss = 1:Plan.NbrSystSetUpScenarios % syst. setup scenarios = ss
            for rs = 1:Plan.NbrRangeScenarios % range scenario = rs
                
                for i = 1:length(Plan.optFunction)
                    if (Plan.optFunction(i).robust == 1)
                        c(s) = c(s) - evalFunction(Plan.optFunction(i),backUp.scenarioDose(s).D);
                    else
                        if (s == s_nominal(i_4D))
                            c(s) = c(s) - evalFunction(Plan.optFunction(i),backUp.scenarioDose(s).D);
                        %else
                        %    c(s) = c(s) - 0;
                        end
                    end
                end
                s = s + 1;
            end
            
        end
    end
end
end
 

% ------------------------------------------------------------------
function g = gradient (x)   
g = [1 zeros(1,length(x) - 1)];
end

% ------------------------------------------------------------------
function J = jacobianstructure ()
J = sparse(ones(Plan.nscenarios,Plan.nominalSpots + 1));
end

% ------------------------------------------------------------------
function J = jacobian (x)
checkBackUp(x);

J = zeros(Plan.nscenarios,Plan.nominalSpots + 1);

% One row for each constraint, one column for each variable.
% Two strategies implemented: A and B. B seems to be slightly faster than A.
s = 1;
for i_4D = 1:Plan.Nbr4DScenarios
    for rr = 1:Plan.NbrRandomScenarios
        for ss = 1:Plan.NbrSystSetUpScenarios % syst. setup scenarios = ss
            for rs = 1:Plan.NbrRangeScenarios % range scenario = rs
                
                J(s,1) = 1; % derivative with respect to the auxiliar variable t
                % initialize fder
                %fder = sparse(Plan.DoseGrid.nvoxels,1); %A
                fder = 0;%B
                for i = 1:length(Plan.optFunction)
                    if (Plan.optFunction(i).robust == 1)
                        %A
                        %fder = fder - sparse(ROI(Plan.optFunction(i).ROIindex).voxelsID,1,evalDerivative(Plan.optFunction(i),backUp.scenarioDose(s).D),Plan.DoseGrid.nvoxels,1);
                        %B
                        tmp = zeros(Plan.DoseGrid.nvoxels,1);
                        tmp(ROI(Plan.optFunction(i).ROIindex).mask1D) = evalDerivative(Plan.optFunction(i),backUp.scenarioDose(s).D);
                        fder = fder - tmp;
                    else
                        if (s == s_nominal(i_4D))
                            %A
                            %fder = fder - sparse(ROI(Plan.optFunction(i).ROIindex).voxelsID,1,evalDerivative(Plan.optFunction(i),backUp.scenarioDose(s_nominal(i_4D)).D),Plan.DoseGrid.nvoxels,1);
                            %B
                            tmp = zeros(Plan.DoseGrid.nvoxels,1);
                            tmp(ROI(Plan.optFunction(i).ROIindex).mask1D) = evalDerivative(Plan.optFunction(i),backUp.scenarioDose(s_nominal(i_4D)).D);
                            fder = fder - tmp;
                        %else
                            %fder = fder - 0;
                        end
                    end 
                end
                %fder = full(fder);%A
                % 
                J(s,2:end) = sparsedoublematrixvectorproduct_fderP_methodA_parallel(Plan.Scenario4D(i_4D).RandomScenario(rr).RangeScenario(rs).P(:,Plan.SystSetUpScenario(ss).wID),fder);
                s = s + 1;
            end  
        end
    end
end

J =  sparse(J);
end


% -----------------------------------------------------------------
function next = getOptVars (t, f, varstruct)
% INPUT: 
% t = current iteration number
% f = current objective function value
% varstruct = struct containing the fields: x, inf_pr,inf_du,mu,d_norm,regularization_size,alpha_du,alpha_pr and ls_trials.

% if t == 0
%     varstruct = warm_start_in.varstruct;
% end % This changes the variables only here inside the function, but the information is lost when we leave it..which is not useful
% ASK IN IPOPT FORUM HOW TO CHANGE THE VALUE OF THESE VARIABLES FOR
% WARM_START.
warm_start_out(1).varstruct = varstruct;

% Terminate the optimzation if requirements* are fullfilled
% if (abs(old_f - f) <= 5e-10)
%         continue = false;
% else
%     old_f = f;
    next = true;
% end

% * the difference between the objective function of two consecutive
% iterations is smaller than a given value.

if (t == wBackUp_counter*wBackUp_Niter)
    
    wBackUp_counter = wBackUp_counter + 1;
    
    %save w to file
    w = varstruct.x(2:end);
    save('w','w');
    
    % Plot target DVH in nominal case  
    if (OptConfig.plotTargetDVH == 1)
        d = backUp.scenarioDose(s_nominal).D(logical(ROI(Plan.TargetROI_ID).mask1D));
        int_bins = min(d):0.005:max(d); %bin centers
        int_hist = hist(d,int_bins); % in the future we have to take into account the relative volume of each voxel (SEE plotQvhGeneral in DPBNmodule)
        DVH = 100/sum(int_hist)*flipdim(cumsum(flipdim((int_hist'),1)),1);
        h = plot(int_bins',DVH,  'g-');
        drawnow;
    end
    
end

if (OptConfig.plotObjective == 1)
    
    o = 0;
    for i = 1:length(Plan.optFunction)
        o = o + evalFunction(Plan.optFunction(i),backUp.scenarioDose(s_nominal).D);
    end
    
    store_fobj(t+1) = o;
    
    % plot objective function output
    figHandles = get(0,'Children');
    if ~isempty(figHandles)
        IdxHandle = strcmp(get(figHandles,'Name'),'Progress of Optimization');
    else
        IdxHandle = [];
    end
    
    if any(IdxHandle)
        figOpt = figHandles(IdxHandle);
        AxesInfigOpt = findall(figOpt,'type','axes');
        set(AxesInfigOpt,'NextPlot', 'replacechildren')
        children = get(AxesInfigOpt,'children');
        delete(children);
    else
        figOpt = figure('Name','Progress of Optimization','NumberTitle','off','Color',[1 1 1]);
        hold on, grid on, grid minor,
        AxesInfigOpt = findall(figOpt,'type','axes');
    end
    
    defaultFontSize = 14;
    set(AxesInfigOpt,'YScale','log');
    title(AxesInfigOpt,'Progress of Optimization','LineWidth',defaultFontSize),
    xlabel(AxesInfigOpt,'# iterations','Fontsize',defaultFontSize),ylabel(AxesInfigOpt,'objective function value','Fontsize',defaultFontSize)
    
    % draw updated axes
    plot(AxesInfigOpt,0:1:t,store_fobj,'xb','LineWidth',1.5);
    drawnow
end

end

% ----------------------------------------------------------------------

function fvalue = evalFunction(optFunction,D)

% This function evaluates the penalty function associated to each ROI 

ROIindex = optFunction.ROIindex;
d = D(ROI(ROIindex).mask1D);

  
if  (optFunction.ID == 1) % 'min'

    tmp = max(0, optFunction.Dref - d);
    fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*sum(tmp.*tmp); % faster

    %diff = optFunction.Dref - d;
    %diff = diff(logical(diff>0));
    %fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*sum(diff.*diff);

elseif (optFunction.ID == 2) % 'max'

    tmp = max(0, d - optFunction.Dref);
    fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*sum(tmp.*tmp); % faster

    %diff = d - optFunction.Dref;
    %diff = diff(logical(diff>0));
    %fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*sum(diff.*diff);

elseif (optFunction.ID == 3) % 'min_mean'

    fvalue = optFunction.impw*max(0,optFunction.Dref - mean(d));

elseif (optFunction.ID == 4) % 'max_mean'

    fvalue = optFunction.impw*max(0,mean(d) - optFunction.Dref);

elseif (optFunction.ID == 5) % 'minDVH'   

    diff = optFunction.Dref - d;
    dvh_values = sort(d,'descend');
    % compute current dose at Vref
    Dc = dvh_values(max([1 round(optFunction.Vref*ROI(ROIindex).nvoxels)])); % 
    diff = diff(d < optFunction.Dref | d > Dc);
    %diff(d > optFunction.Dref | d < Dc) = 0;
    fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*(diff'*diff);

elseif (optFunction.ID == 6) % 'maxDVH'  
    
     diff = d - optFunction.Dref;
     dvh_values = sort(d,'descend');
     % compute current dose at Vref
     Dc = dvh_values(max([1 round(optFunction.Vref*ROI(ROIindex).nvoxels)])); % 
     diff = diff(d > optFunction.Dref | d < Dc);
     %diff(d < optFunction.Dref | d > Dc) = 0;
     fvalue = (optFunction.impw/ROI(ROIindex).nvoxels)*(diff'*diff); % PROBLEM
end
    
end

% ---------------------------------------------------------------------

function fder = evalDerivative(optFunction,D)
ROIindex = optFunction.ROIindex;
d =D(ROI(ROIindex).mask1D);


if (optFunction.ID == 1) % 'min'

    diff = optFunction.Dref - d;
    diff (diff < 0) = 0;
    fder = (optFunction.impw/ROI(ROIindex).nvoxels)*(-2 * diff);

elseif (optFunction.ID == 2) % 'max'

    diff = d - optFunction.Dref;
    diff(diff < 0) = 0;
    fder = (optFunction.impw/ROI(ROIindex).nvoxels)*(2 * diff);

elseif (optFunction.ID == 3) % 'min_mean'

    fder = (optFunction.impw/ROI(ROIindex).nvoxels)*ones(ROI(ROIindex).nvoxels,1);

elseif (optFunction.ID == 4) % 'max_mean'

    fder = (optFunction.impw/ROI(ROIindex).nvoxels)*ones(ROI(ROIindex).nvoxels,1);

elseif (optFunction.ID == 5) % 'minDVH'

     diff = optFunction.Dref - d;
     dvh_values = sort(d,'descend');
     % compute current dose at Vref
     Dc = dvh_values(max([1 round(optFunction.Vref*ROI(ROIindex).nvoxels)])); 
     diff(d > optFunction.Dref | d < Dc) = 0; % need to have the same size as mask1D
     fder = (optFunction.impw/ROI(ROIindex).nvoxels)*(-2* diff);

elseif (optFunction.ID == 6) % 'maxDVH'

     diff = d - optFunction.Dref;
     dvh_values = sort(d,'descend');
     % compute current dose at Vref
     Dc = dvh_values(max([1 round(optFunction.Vref*ROI(ROIindex).nvoxels)])); 
     diff(d > optFunction.Dref | d < Dc) = 0; % need to have the same size as mask1D
     fder = (optFunction.impw/ROI(ROIindex).nvoxels)*(2* diff); %PROBLEM

end
end

% ---------------------------------------------------------------------

% ----------------------------------------------------------------------



end